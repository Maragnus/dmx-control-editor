﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DmxControlEditor
{
    class CustomFlowLayoutPanel : System.Windows.Forms.FlowLayoutPanel
    {
        protected override System.Drawing.Point ScrollToControl(System.Windows.Forms.Control activeControl)
        {
            var defaultValue = base.ScrollToControl(activeControl);
            return new System.Drawing.Point(this.DisplayRectangle.Location.X, defaultValue.Y);
        }
    }
}
