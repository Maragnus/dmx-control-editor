﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using Color = System.Drawing.Color;

namespace DmxControlEditor
{
    namespace DmxJson
    {
        [DataContract]
        public class DMX_ROOT
        {
            [DataMember] public IList<DMX_DEVICE> devices;
            [DataMember] public IList<DMX_TRIGGER> triggers;
            [DataMember] public IList<DMX_ACTION> actions;

            public static DMX_ROOT DeserializeFromFile(string fileName)
            {
                var root = new DMX_ROOT();
                using (var stream = new System.IO.FileStream(fileName, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                {
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(DMX_ROOT));
                    root = (DMX_ROOT)serializer.ReadObject(stream);
                    return root;
                }
            }

            public static DMX_ROOT Deserialize(string json)
            {
                var root = new DMX_ROOT();
                using (var stream = new System.IO.MemoryStream(Encoding.Unicode.GetBytes(json)))
                {
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(DMX_ROOT));
                    root = (DMX_ROOT)serializer.ReadObject(stream);
                    return root;
                }
            }

            public static string Serialize(DMX_ROOT root)
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(DMX_ROOT));
                using (var stream = new System.IO.MemoryStream())
                {
                    serializer.WriteObject(stream, root);
                    return Encoding.Default.GetString(stream.ToArray());
                }
            }
        }

        [DataContract]
        public class DMX_DEVICE
        {
            [DataMember] public string id;
            [DataMember] public string type;
            [DataMember] public string address;
            [DataMember] public string config;
        }

        [DataContract]
        public class DMX_TRIGGER
        {
            [DataMember] public string id;
            [DataMember] public string state;
            [DataMember] public double valuelow;
            [DataMember] public double valuehigh;
            [DataMember] public string action;
            [DataMember] public string msg;             
        }

        [DataContract]
        public class DMX_ACTION
        {
            [DataMember] public string id;
            [DataMember] public List<DMX_COMMAND> commands;
        }

        [DataContract]
        public class DMX_COMMAND
        {
            [DataMember] public string what;
            [DataMember] public string color;
            [DataMember] public double time;
            [DataMember] public bool wait;
            [DataMember] public string colorstart;
            [DataMember] public string colorend;
        }


    }

    namespace DmxXml
    {
        public class DMX_SETVALUE
        {
            [XmlAttribute]
            public int change;
            [XmlAttribute]
            public int value;
            [XmlAttribute]
            public int index;
        }

        public class DMX_TIMEBLOCK
        {
            [XmlAttribute]
            public int mseconds;

            [XmlElement("setvalue")]
            public List<DMX_SETVALUE> setvalues = new List<DMX_SETVALUE>(3);
        }

        public class DMX_EVENT
        {
            [XmlAttribute]
            public string type;

            [XmlAttribute]
            public string continuous;

            [XmlElement("timeblock")]
            public List<DMX_TIMEBLOCK> timeblocks = new List<DMX_TIMEBLOCK>(2);
        }

        public class DMX_CONTROL
        {
            [XmlAttribute]
            public string version;

            [XmlElement("event")]
            public List<DMX_EVENT> events = new List<DMX_EVENT>(16);

            public static DMX_CONTROL CreateFrom(string fileName)
            {
                using (var stream = new System.IO.FileStream(fileName, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                {
                    return CreateFrom(stream);
                }
            }

            public static DMX_CONTROL CreateFrom(System.IO.Stream stream)
            {
                var serializer = new XmlSerializer(typeof(DMX_CONTROL));
                return (DMX_CONTROL)serializer.Deserialize(stream);
            }

            internal void Save(string fileName)
            {
                using (var stream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.Write))
                {
                    var serializer = new XmlSerializer(typeof(DMX_CONTROL));
                    serializer.Serialize(stream, this);
                }
            }
        }
    }

    public class DmxBase : INotifyPropertyChanged
    {
        public DmxControl Control { get; private set; }

        protected DmxBase(DmxControl control)
        {
            this.Control = control == null ? control as DmxControl : control;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyChange(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class DmxValue : DmxBase
    {
        private bool present = false;
        private int value = 0;
        private int change = 0;

        public DmxTimeBlock TimeBlock { get; private set; }
        public int Index { get; private set; }
        public bool Present { get { return present; } set { present = value; NotifyChange("Present"); } }
        public int Value { get { return value; } set { this.value = value; NotifyChange("Value"); } }
        public int Change { get { return change; } set { change = value; NotifyChange("Change"); } }

        public DmxValue(DmxTimeBlock timeBlock, int index) : base(timeBlock.Control)
        {
            this.TimeBlock = timeBlock;
            this.Index = index;
        }

        public DmxValue(DmxValue o) : this(o.TimeBlock, o.Index)
        {
            present = o.present;
            value = o.value;
            change = o.change;
        }
    }

    public class DmxTimeBlock : DmxBase
    {
        private int mseconds = 0;

        public DmxEvent Event { get; private set; }
        public int Milliseconds { get { return mseconds; } set { mseconds = value; NotifyChange("Milliseconds"); } }
        public int StartTime { get; set; }

        public ObservableCollection<DmxValue> Values { get; private set; }
        public DmxValue this[int index]
        {
            get
            {
                var val = Values.FirstOrDefault(m => m.Index == index);
                if (val == null)
                    Values.Add(val = new DmxValue(this, index));
                return val;
            }
        }

        public DmxTimeBlock(DmxTimeBlock o) : this(o.Event)
        {
            mseconds = o.mseconds;
            foreach (var v in o.Values)
                Values.Add(new DmxValue(v));
        }


        public DmxTimeBlock(DmxEvent ev) : base(ev.Control)
        {
            this.Event = ev;
            this.Values = new ObservableCollection<DmxValue>();
            this.Values.CollectionChanged += SetValue_CollectionChanged;
        }

        private void SetValue_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
                foreach (var item in e.OldItems)
                    (item as DmxValue).PropertyChanged -= DmxSetValue_PropertyChanged;

            if (e.NewItems != null)
                foreach (var item in e.NewItems)
                    (item as DmxValue).PropertyChanged += DmxSetValue_PropertyChanged;
            NotifyChange("SetValues");
        }

        void DmxSetValue_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            NotifyChange("SetValue");
        }
    }
    public class DmxEvent : DmxBase
    {
        private bool continuous = true;

        public string Name { get; private set; }
        public bool Continuous { get { return continuous; } set { continuous = value; NotifyChange("Continuous"); } }
        public ObservableCollection<DmxTimeBlock> TimeBlocks { get; private set; }
        public int Duration { get; private set; }

        public DmxEvent(DmxControl control, string name) : base(control)
        {
            this.Name = name;
            this.TimeBlocks = new ObservableCollection<DmxTimeBlock>();
            this.TimeBlocks.CollectionChanged += TimeBlocks_CollectionChanged;
        }

        internal void RecalculateStartTime()
        {
            var duration = 0;

            foreach (var tb in TimeBlocks)
            {
                tb.StartTime = duration;
                duration += tb.Milliseconds;
            }
            
            if (duration != Duration)
            {
                Duration = duration;
                NotifyChange("Duration");
            }
        }

        private void TimeBlocks_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
                foreach (var item in e.OldItems)
                    (item as DmxTimeBlock).PropertyChanged -= DmxEvent_PropertyChanged;

            if (e.NewItems != null)
                foreach (var item in e.NewItems)
                    (item as DmxTimeBlock).PropertyChanged += DmxEvent_PropertyChanged;

            RecalculateStartTime();
            NotifyChange("TimeBlocks");
        }

        void DmxEvent_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Milliseconds")
                RecalculateStartTime();
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public enum ModuleType
    {
        None,
        Single,
        RGB,
        GRB,
    }

    public class DmxXmlModules
    {
        private List<DmxXmlModule> modules = new List<DmxXmlModule>();

        [XmlElement]
        public List<DmxXmlModule> Modules { get { return modules; } }
    }
    public class DmxXmlModule
    {
        [XmlAttribute]
        public ModuleType type;
        [XmlAttribute]
        public int index1;
        [XmlAttribute]
        public int index2;
        [XmlAttribute]
        public int index3;
        [XmlAttribute]
        public string color;
        [XmlAttribute]
        public string name;
    }

    public class DmxModule : DmxBase
    {
        private ModuleType type;
        private int index1;
        private int index2;
        private int index3;
        private Color color;
        private string name;
        
        public ModuleType Type { get { return type; } set { type = value; NotifyChange("Type"); } }
        public string Name { get { return name; } set { name = value; NotifyChange("Name"); } }
        public Color Color { get { return type == ModuleType.RGB || type == ModuleType.GRB ? Color.White : color; } set { color = value; NotifyChange("Color"); } }
        public int Index1 { get { return index1; } set { index1 = value; NotifyChange("Index1"); } }
        public int Index2 { get { return index2; } set { index2 = value; NotifyChange("Index2"); } }
        public int Index3 { get { return index3; } set { index3 = value; NotifyChange("Index3"); } }

        public DmxModule(DmxControl control)
            : base(control)
        {

        }

        internal static DmxModule FromXml(DmxControl control, DmxXmlModule xml)
        {
            return new DmxModule(control)
            {
                type = xml.type,
                index1 = xml.index1,
                index2 = xml.index2,
                index3 = xml.index3,
                color = System.Drawing.ColorTranslator.FromHtml(xml.color),
                name = xml.name
            };
        }

        internal DmxXmlModule ToXml()
        {
            return new DmxXmlModule()
            {
                type = type,
                index1 = index1,
                index2 = index2,
                index3 = index3,
                color = System.Drawing.ColorTranslator.ToHtml(color),
                name = name
            };
        }

        public Color GetStartColor(DmxTimeBlock block)
        {
            switch (type)
            {
                case ModuleType.None:
                    return Color.Transparent;

                case ModuleType.Single:
                    var setvalue = block.Values.FirstOrDefault(m => m.Index == index1);
                    if (setvalue == null)
                        return Color.Transparent;
                    var intensity = (setvalue.Value / 255.0d);
                    return Color.FromArgb(
                            (int)(color.R * intensity),
                            (int)(color.G * intensity),
                            (int)(color.B * intensity));

                case ModuleType.RGB:
                case ModuleType.GRB:
                    var setvalue1 = block.Values.FirstOrDefault(m => m.Index == index1);
                    var setvalue2 = block.Values.FirstOrDefault(m => m.Index == index2);
                    var setvalue3 = block.Values.FirstOrDefault(m => m.Index == index3);
                    if ((setvalue1 == null || !setvalue1.Present) && (setvalue2 == null || !setvalue2.Present) && (setvalue3 == null || !setvalue3.Present))
                        return Color.Transparent;

                    var red = (setvalue1 == null || !setvalue1.Present) ? 0 : (setvalue1.Value);
                    var green = (setvalue2 == null || !setvalue2.Present) ? 0 : (setvalue2.Value);
                    var blue = (setvalue3 == null || !setvalue3.Present) ? 0 : (setvalue3.Value);

                    var redIntensity = red / 255.0d;
                    var greenIntensity = green / 255.0d;
                    var blueIntensity = blue / 255.0d;

                    return Color.FromArgb(
                        Math.Min(255, Math.Max(0, (int)(255 * redIntensity))),
                        Math.Min(255, Math.Max(0, (int)(255 * greenIntensity))),
                        Math.Min(255, Math.Max(0, (int)(255 * blueIntensity))));

                default:
                    return Color.Transparent;
            }
        }

        public Color GetEndColor(DmxTimeBlock block)
        {
            switch (type)
            {
                case ModuleType.None:
                    return Color.Transparent;

                case ModuleType.Single:
                    var setvalue = block.Values.FirstOrDefault(m => m.Index == index1);
                    if (setvalue == null)
                        return Color.Transparent;
                    var intensity = Math.Min(1, Math.Max(0, (setvalue.Value + setvalue.Change) / 255.0d));
                    return Color.FromArgb(
                            (int)(color.R * intensity),
                            (int)(color.G * intensity),
                            (int)(color.B * intensity));

                case ModuleType.RGB:
                case ModuleType.GRB:
                    var setvalue1 = block.Values.FirstOrDefault(m => m.Index == index1);
                    var setvalue2 = block.Values.FirstOrDefault(m => m.Index == index2);
                    var setvalue3 = block.Values.FirstOrDefault(m => m.Index == index3);
                    if ((setvalue1 == null || !setvalue1.Present) && (setvalue2 == null || !setvalue2.Present) && (setvalue3 == null || !setvalue3.Present))
                        return Color.Transparent;

                    var red = (setvalue1 == null || !setvalue1.Present) ? 0 : (setvalue1.Value + setvalue1.Change);
                    var green = (setvalue2 == null || !setvalue2.Present) ? 0 : (setvalue2.Value + setvalue2.Change);
                    var blue = (setvalue3 == null || !setvalue3.Present) ? 0 : (setvalue3.Value + setvalue3.Change);

                    var redIntensity = red / 255.0d;
                    var greenIntensity = green / 255.0d;
                    var blueIntensity = blue / 255.0d;

                    return Color.FromArgb(
                        Math.Min(255, Math.Max(0, (int)(255 * redIntensity))),
                        Math.Min(255, Math.Max(0, (int)(255 * greenIntensity))),
                        Math.Min(255, Math.Max(0, (int)(255 * blueIntensity))));

                default:
                    return Color.Transparent;
            }
        }

        public Color GetColor(Dictionary<int, byte> values)
        {
            switch (type)
            {
                case ModuleType.None:
                    return Color.Transparent;
                case ModuleType.Single:
                    var intensity = values.ContainsKey(index1) ? (values[index1] / 255.0d) : 0;
                    return Color.FromArgb(
                            (int)(color.R * intensity),
                            (int)(color.G * intensity),
                            (int)(color.B * intensity));
                case ModuleType.RGB:
                case ModuleType.GRB:
                    var redIntensity = values.ContainsKey(index1) ? Math.Min(255, Math.Max(0, (values[index1] / 255.0d))) : 0;
                    var greenIntensity = values.ContainsKey(index2) ? Math.Min(255, Math.Max(0, (values[index2] / 255.0d))) : 0;
                    var blueIntensity = values.ContainsKey(index3) ? Math.Min(255, Math.Max(0, (values[index3] / 255.0d))) : 0;
                    return Color.FromArgb(
                        (int)(255 * redIntensity),
                        (int)(255 * greenIntensity),
                        (int)(255 * blueIntensity));
                default:
                    return Color.Transparent;
            }
        }
    }


    public class DmxControl : DmxBase
    {
        const string DmxVersion = "2.00";
        const string DmxCommandNames = "PLAYER_SHIELDS_RAISED PLAYER_SHIELDS_ON PLAYER_SHIELDS_LOWERED PLAYER_SHIELDS_ON RED_ALERT JUST_KILLED_DAMCON_MEMBER SELF_DESTRUCTED SHIP_DAMAGE_20 SHIP_DAMAGE_40 SHIP_DAMAGE_60 COMPLETELY_DOCKED TRACTORED_FOR_DOCKED COMPLETELY_DOCKED JUMP_INITIATED JUMP_EXECUTED JUMP_FIZZLED FRONT_SHIELD_LOW REAR_SHIELD_LOW ENTERING_NEBULA WITHIN_NEBULA EXITING_NEBULA START_DOCKING TRACTORED_FOR_DOCKED HELM_IN_REVERSE SOMETHING_HITS_PLAYER NPC_BEAM_HITS_PLAYER PLAYER_BEAM_HITS_PLAYER TORPEDO_HITS_PLAYER MINE_HITS_PLAYER LIGHTNING_HITS_PLAYER COLLISION_HITS_PLAYER DRONE_HITS_PLAYER PLAYER_TAKES_INTERNAL_DAMAGE PLAYER_TAKES_SHIELD_DAMAGE PLAYER_TAKES_FRONT_SHIELD_DAMAGE PLAYER_TAKES_REAR_SHIELD_DAMAGE DAMCON_CASUALTY SHIP_DAMAGE_20 SHIP_DAMAGE_40 SHIP_DAMAGE_60 PLAYER_DESTROYED GAME_OVER ENERGY_LOW ENERGY_20 ENERGY_40 ENERGY_60 ENERGY_80 ENERGY_100 ENERGY_200 UNLOADING_TUBE UNLOADING_TUBE1 UNLOADING_TUBE2 UNLOADING_TUBE3 UNLOADING_TUBE4 LOADING_TUBE LOADING_TUBE1 LOADING_TUBE2 LOADING_TUBE3 LOADING_TUBE4 ANY_TUBE_READY_TO_FIRE ANY_TUBE_EMPTY FINISHED_LOADING_TUBE1 FINISHED_LOADING_TUBE2 FINISHED_LOADING_TUBE3 FINISHED_LOADING_TUBE4 FINISHED_UNLOADING_TUBE1 FINISHED_UNLOADING_TUBE2 FINISHED_UNLOADING_TUBE3 FINISHED_UNLOADING_TUBE4 NUKE_READY_TO_FIRE TORP_HOMING_FIRED TORP_NUKE_FIRED TORP_MINE_FIRED TORP_EMP_FIRED";
        public static readonly IReadOnlyCollection<string> DmxCommandList;
        static DmxControl()
        {
            DmxCommandList = DmxCommandNames.Split(new char[] { ' ' }).ToList().AsReadOnly();
        }

        public DmxControl() : base(null)
        {
            Events = new ObservableCollection<DmxEvent>();
            Events.CollectionChanged += Events_CollectionChanged;
            modules = new List<DmxModule>(8);
            modules.Add(new DmxModule(this) { Type = ModuleType.RGB, Index1 = 0, Index2 = 1, Index3 = 2, Color = Color.White, Name = "P" });
            ftdi = FtdiDmx.Create();
            ftdi.QueryValues += FtdiDmx_QueryValues;
            version = DmxVersion;
        }

        void FtdiDmx_QueryValues(object sender, FtdiDmxQueryValues e)
        {
            var time = (int)(DateTime.Now - ftdiStart).TotalMilliseconds;
            var values = GetValues(time);
            for (int index = 0; index < e.Values.Length; index++)
            {
                byte value = 0;
                values.TryGetValue(index, out value);
                e.Values[index] = value;
            }            
        }

        void Events_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null && e.OldItems.Contains(PreviewEvent))
                PreviewEvent = null;
            if (e.OldItems != null && e.OldItems.Contains(RunningEvent))
                RunningEvent = null;
        }

        private string version;
        private DmxEvent previewEvent = null;
        private DmxEvent runningEvent = null;
        private FtdiDmx ftdi;
        private DateTime ftdiStart;
        private List<DmxModule> modules;

        public string Version { get { return version; } }
        public ObservableCollection<DmxEvent> Events { get; private set; }
        public ReadOnlyCollection<DmxModule> Modules { get { return modules.AsReadOnly(); } }
        public DmxEvent PreviewEvent { get { return previewEvent; } set { previewEvent = value; NotifyChange("PreviewEvent"); } }
        public DmxEvent RunningEvent { get { return runningEvent; } set { runningEvent = value; NotifyChange("RunningEvent"); } }

        public void AddModule(DmxModule module)
        {
            modules.Add(module);
            module.PropertyChanged += Module_PropertyChanged;
            NotifyChange("Modules");
        }

        void Module_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var count = 0;
            foreach (var module in modules) 
            {
                if (module.Type == ModuleType.RGB || module.Type == ModuleType.GRB)
                    count = Math.Max(Math.Max(Math.Max(count, module.Index1), module.Index2), module.Index3);
                else if (module.Type == ModuleType.Single)
                    count = Math.Max(count, module.Index1);
            }
            ftdi.ChannelCount = count + 1;

            if (e != null)
                NotifyChange("Module");
        }

        public void RemoveModule(DmxModule module)
        {
            module.PropertyChanged -= Module_PropertyChanged;
            modules.Remove(module);
            NotifyChange("Modules");
        }

        public bool DmxRunning
        {
            get
            {
                return ftdi.State != FtdiDmxState.Closed;
            }
            set
            {
                if (value)
                    StartDmx();
                else
                    StopDmx();
                NotifyChange("DmxRunning");
            }
        }

        private void StartDmx()
        {
            Module_PropertyChanged(this, null);
            if (ftdi.State == FtdiDmxState.Closed)
                ftdi.Open();
            if (ftdi.State == FtdiDmxState.Open)
                ftdi.Writing = true;
            ftdiStart = DateTime.Now;
        }

        private void StopDmx()
        {
            if (ftdi.State != FtdiDmxState.Closed)
                ftdi.Close();
        }

        private void CalculateValues(Dictionary<int, byte> values, int time, DmxEvent ev)
        {
            var duration = ev.Duration + (ev.Continuous ? 0 : 1000);
            if (duration == 0)
                return;

            var eventTime = time % duration;
            var timeBlock = ev.TimeBlocks.FirstOrDefault(m => eventTime <= m.StartTime + m.Milliseconds);
            if (timeBlock != null)
            {
                // Calculate the progress of a Change attribute
                var progress = (double)Math.Min(1000, eventTime - timeBlock.StartTime) / 1000.0d;
                
                foreach (var v in timeBlock.Values)
                {
                    if (v.Present)
                    {
                        var value = v.Value;                        
                        if (v.Change != 0)
                            value += (int)(v.Change * progress);
                        value = Math.Min(255, Math.Max(0, value));                       
                        values[v.Index] = (byte)value;
                    }

                }
            }
        }

        public void SaveModules(bool saveToDisk = true)
        {
            using (var stream = new System.IO.MemoryStream(1024))
            {
                SaveModulesToStream(stream);
                Properties.Settings.Default.Modules = Encoding.UTF8.GetString(stream.GetBuffer());
                if (saveToDisk)
                    Properties.Settings.Default.Save();                
            }
        }

        public void SaveModulesToFile(string fileName)
        {
            using (var stream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.Write))
            {
                SaveModulesToStream(stream);
            }
        }

        public void SaveModulesToStream(System.IO.Stream stream)
        {
            var serializer = new XmlSerializer(typeof(DmxXmlModules));
            var modules = new DmxXmlModules();
            var moduleList = (from module in Modules select module.ToXml());
            modules.Modules.AddRange(moduleList);
            serializer.Serialize(stream, modules);
        }

        public void LoadModules()
        {
            try
            {
                var data = Encoding.UTF8.GetBytes(Properties.Settings.Default.Modules);
                using (var stream = new System.IO.MemoryStream(data))
                {
                    LoadModulesFromStream(stream);
                }
            }
            catch (Exception ex)
            {
                // Do nothing...
            }
        }


        public void LoadModulesFromFile(string fileName)
        {
            using (var stream = new System.IO.FileStream(fileName, System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                LoadModulesFromStream(stream);
            }
        }

        public void LoadModulesFromStream(System.IO.Stream stream)
        {
            var serializer = new XmlSerializer(typeof(DmxXmlModules));
            var modules = serializer.Deserialize(stream) as DmxXmlModules;
            foreach (var module in this.modules.ToArray())
                RemoveModule(module);
            foreach (var xml in modules.Modules)
                AddModule(DmxModule.FromXml(this, xml));
        }

        public Dictionary<int, byte> GetValues(int time)
        {
            var values = new Dictionary<int, byte>();

            // Reset all modules to black
            foreach (var module in Modules)
            {
                if (module.Type == ModuleType.Single)
                {
                    values[module.Index1] = 0;
                }
                else if (module.Type == ModuleType.RGB || module.Type == ModuleType.GRB)
                {
                    values[module.Index1] = 0;
                    values[module.Index2] = 0;
                    values[module.Index3] = 0;
                }
            }

            if (runningEvent != null)
                CalculateValues(values, time, runningEvent);

            if (previewEvent != null)
                CalculateValues(values, time, previewEvent);

            return values;
        }

        public static DmxControl CreateFrom(string fileName)
        {
            using (var stream = new System.IO.FileStream(fileName, System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                return CreateFrom(stream);
            }
        }

        public static DmxControl CreateFrom(System.IO.Stream stream)
        {
            var serializer = new XmlSerializer(typeof(DmxXml.DMX_CONTROL));
            var control = serializer.Deserialize(stream) as DmxXml.DMX_CONTROL;
            return CreateFrom(control);
        }

        public static DmxControl CreateFrom(DmxXml.DMX_CONTROL xml)
        {
            var control = new DmxControl();
            control.version = xml.version;
            foreach (var xev in xml.events)
            {
                var ev = new DmxEvent(control, xev.type)
                {
                    Continuous = String.Compare(xev.continuous, "yes", true) == 0
                };

                foreach (var xtb in xev.timeblocks)
                {
                    var tb = new DmxTimeBlock(ev)
                    {
                        Milliseconds = xtb.mseconds
                    };

                    foreach(var xsv in xtb.setvalues)
                    {
                        tb.Values.Add(new DmxValue(tb, xsv.index)
                        {
                            Present = true,
                            Value = xsv.value,
                            Change = xsv.change
                        });
                    }

                    ev.TimeBlocks.Add(tb);
                }
                control.Events.Add(ev);
            }

            foreach (var name in DmxControl.DmxCommandList)
            {
                if (!control.Events.Any(m => m.Name == name))
                    control.Events.Add(new DmxEvent(control, name));
            }

            control.RunningEvent = control.Events.FirstOrDefault(ev => ev.Name == "NORMAL_CONDITION_1");
            return control;
        }
        
        public DmxXml.DMX_CONTROL ToDmxXml()
        {            
#if !DEMO
            var xml = new DmxXml.DMX_CONTROL();
            xml.version = Version;
            foreach (var e in Events.Where(e => e.TimeBlocks.Count > 0))
            {
                var xmlEvent = new DmxXml.DMX_EVENT();
                xmlEvent.continuous = e.Continuous ? "yes" : "no";
                xmlEvent.type = e.Name;
                foreach (var tb in e.TimeBlocks)
                {
                    var xmlTimeBlock = new DmxXml.DMX_TIMEBLOCK();
                    xmlTimeBlock.mseconds = tb.Milliseconds;
                    foreach(var sv in tb.Values.Where(sv=>sv.Present).OrderBy(sv => sv.Index))
                    {                        
                        var xmlSetValue = new DmxXml.DMX_SETVALUE();
                        xmlSetValue.index = sv.Index;
                        xmlSetValue.value = sv.Value;
                        xmlSetValue.change = sv.Change;
                        xmlTimeBlock.setvalues.Add(xmlSetValue);
                    }
                    xmlEvent.timeblocks.Add(xmlTimeBlock);
                }
                xml.events.Add(xmlEvent);
            }

            return xml;
#else
            throw new NotImplementedException("Saving is not implemented in this version.");            
#endif
        }

        internal void Save(string fileName)
        {
            var serializer = new XmlSerializer(typeof(DmxXml.DMX_CONTROL), "");
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            using (var stream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.Write))
            {
                var settings = new XmlWriterSettings()
                {
                    Encoding = System.Text.Encoding.UTF8,
                    Indent = true
                };
                using (var writer = XmlWriter.Create(stream, settings))
                {
                    serializer.Serialize(writer, ToDmxXml(), ns);
                }
            }
        }

        public int GetLastIndex()
        {
            int index = -1;
            foreach (var module in modules)
            {
                if (module.Type == ModuleType.Single)
                {
                    index = Math.Max(module.Index1, index);
                }
                else if (module.Type == ModuleType.RGB || module.Type == ModuleType.GRB)
                {
                    index = Math.Max(Math.Max(Math.Max(module.Index1, module.Index2), module.Index3), index);
                }
            }
            return index;
        }
    }
}
