﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace DmxControlEditor
{
    public partial class EventEditor : UserControl
    {
        private DmxEvent dmxEvent;
        public DmxEvent DmxEvent
        {
            get
            {
                return dmxEvent;
            }
            set
            {
                if (DmxEvent != null)
                    DmxEvent.PropertyChanged -= DmxEvent_PropertyChanged;
                dmxEvent = value;
                if (DmxEvent != null)
                    DmxEvent.PropertyChanged += DmxEvent_PropertyChanged;
                UpdateEverything();
            }
        }

        public EventEditor()
        {
            InitializeComponent();
        }

        private void UpdateEverything()
        {
            if (DmxEvent == null)
            {
                eventNameLabel.Text = "(No selection)";
                flowLayout.Controls.Clear();
                continuous.Enabled = false;
                return;
            }
            SuspendLayout();
            var textInfo = new CultureInfo("en-US", false).TextInfo;
            eventNameLabel.Text = textInfo.ToTitleCase(DmxEvent.Name.Replace('_', ' ').ToLower());
            continuous.Checked = DmxEvent.Continuous;
            continuous.Enabled = true;
            flowLayout.Visible = false;
            flowLayout.TabStop = true;
            flowLayout.Controls.Clear();
            foreach (var tb in DmxEvent.TimeBlocks)
            {
                var tbEditor = new TimeBlockEditor(tb);
                tbEditor.MoveUpPressed += TimeBlock_MoveUpPressed;
                tbEditor.MoveDownPressed += TimeBlock_MoveDownPressed;
                tbEditor.DeletePressed += TimeBlock_DeletePressed;
                tbEditor.DuplicatePressed += TimeBlock_DuplicatePressed;
                flowLayout.Controls.Add(tbEditor);                
            }
            flowLayout.Visible = true;

            var addButton = new Button()
            {
                Text = "Add"
            };
            addButton.Click += AddButton_Click;
            flowLayout.Controls.Add(addButton);

            UpdateOrder();
            ResumeLayout();
        }

        void AddButton_Click(object sender, EventArgs e)
        {
            DmxEvent.TimeBlocks.Add(new DmxTimeBlock(DmxEvent) { Milliseconds = 1000 });
            UpdateEverything();
        }

        void TimeBlock_DuplicatePressed(object sender, EventArgs e)
        {
            var original = (sender as TimeBlockEditor).TimeBlock;
            var index = DmxEvent.TimeBlocks.IndexOf(original);
            DmxEvent.TimeBlocks.Insert(index, new DmxTimeBlock(original));
            UpdateEverything();
        }

        void TimeBlock_DeletePressed(object sender, EventArgs e)
        {
            var original = (sender as TimeBlockEditor).TimeBlock;
            DmxEvent.TimeBlocks.Remove(original);
            UpdateEverything();
        }

        void TimeBlock_MoveDownPressed(object sender, EventArgs e)
        {
            var original = (sender as TimeBlockEditor).TimeBlock;
            var index = DmxEvent.TimeBlocks.IndexOf(original);
            DmxEvent.TimeBlocks.Remove(original);
            DmxEvent.TimeBlocks.Insert(index+1, original);
            UpdateEverything();
        }

        void TimeBlock_MoveUpPressed(object sender, EventArgs e)
        {
            var original = (sender as TimeBlockEditor).TimeBlock;
            var index = DmxEvent.TimeBlocks.IndexOf(original);
            DmxEvent.TimeBlocks.Remove(original);
            DmxEvent.TimeBlocks.Insert(index -1, original);
            UpdateEverything();
        }

        private void UpdateOrder()
        {
            var count = flowLayout.Controls.Count;
            var tbs = new List<TimeBlockEditor>(count);
            for (var index = 0; index < count; index++)
            {
                var editor = flowLayout.Controls[index] as TimeBlockEditor;
                if (editor != null)
                    tbs.Add(editor);
            }

            var first = tbs.FirstOrDefault();
            var last = tbs.LastOrDefault();
            foreach (var tb in tbs) 
            {
                tb.MoveUpEnabled = tb != first;
                tb.MoveDownEnabled = tb != last;
            }
        }

        void DmxEvent_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
           
        }

        private void continuous_CheckedChanged(object sender, EventArgs e)
        {
            dmxEvent.Continuous = continuous.Checked;
        }

        private void flowLayout_Scroll(object sender, ScrollEventArgs e)
        {

        }
    }
}
