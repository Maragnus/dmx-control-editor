﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace DmxControlEditor
{
    public enum FtdiDmxState
    {
        Closed,
        Open,
        Running
    }

    public class FtdiDmxException : Exception
    {
        public FtdiDmxException(string message)
            : base(message + (IntPtr.Size == 8 ? " (x64)" : " (x86)"))
        {

        }
        public FtdiDmxException(string message, Exception innerException)
            : base(message + (IntPtr.Size == 8 ? " (x64)" : " (x86)"), innerException)
        {

        }
    }

    public class FtdiDmxQueryValues : EventArgs
    {
        public byte[] Values { get; private set; }

        public FtdiDmxQueryValues(int channels)
        {
            Values = new byte[channels];
        }
    }

    public abstract class FtdiDmx : IDisposable
    {
        public abstract void Open();
        public abstract void Close();

        public event EventHandler<FtdiDmxQueryValues> QueryValues;

        internal FtdiDmx()
        { }

        public FtdiDmxState State { get; protected set; }

        public static FtdiDmx Create()
        {
            if (IntPtr.Size == 8)
                return new FtdiDmx64();
            else
                return new FtdiDmx32();
        }

        private Thread writer;
        private System.Threading.CancellationTokenSource cancel;

        public bool Writing
        {
            get
            {
                return State == FtdiDmxState.Running;
            }
            set
            {
                if (value)
                    StartRunning();
                else
                    StopRunning();
            }
        }

        protected void StartRunning()
        {
            if (State == FtdiDmxState.Running)
                return;
            if (State == FtdiDmxState.Closed)
                throw new FtdiDmxException("Cannot run when closed");

            if (cancel != null)
                cancel.Dispose();
            cancel = new System.Threading.CancellationTokenSource();

            writer = new Thread(WriteProc);
            writer.IsBackground = true;
            State = FtdiDmxState.Running;
            writer.Start();
        }

        protected void StopRunning()
        {
            if (State == FtdiDmxState.Closed)
                return;
            if (State != FtdiDmxState.Running)
                return;
            if (writer == null)
                return;

            cancel.Cancel();

            writer.Join();
            writer = null;

            cancel.Dispose();
            cancel = null;

            State = FtdiDmxState.Open;
        }

        protected abstract int WriteData(IntPtr dataPtr, int dataLen);

        private void WriteProc()
        {
            int lastDataLen = ChannelCount, dataLen = 0;
            IntPtr dataPtr = IntPtr.Zero;

            try
            {
                while (!cancel.IsCancellationRequested)
                {
                    dataLen = ChannelCount;
                    if (dataLen > lastDataLen)
                    {
                        Marshal.FreeHGlobal(dataPtr);
                        dataPtr = IntPtr.Zero;
                    }
                    if (dataPtr == IntPtr.Zero)
                    {
                        dataPtr = Marshal.AllocHGlobal(dataLen + 1);
                        Marshal.WriteByte(dataPtr, 0);
                    }
                    lastDataLen = dataLen;

                    if (QueryValues != null)
                    {
                        var e = new FtdiDmxQueryValues(dataLen);
                        QueryValues(this, e);
                        lock (values)
                            e.Values.CopyTo(values, 0);
                        Marshal.Copy(e.Values, 0, dataPtr+1, dataLen);                        
                    }
                    else
                    { 
                        lock (values)
                            Marshal.Copy(values, 0, dataPtr+1, dataLen);
                    }

                    WriteData(dataPtr, dataLen + 1);

                    WaitHandle.WaitAny(new WaitHandle[] { cancel.Token.WaitHandle }, 20);
                }
            }
            finally
            {
                 Marshal.FreeHGlobal(dataPtr);
            }
        }

        public void Dispose()
        {
            StopRunning();
            Close();
        }

        private int channels = 3;

        public int ChannelCount
        {
            get
            {
                return channels;
            }
            set
            {
                if (value <= 0 || value > 511)
                    throw new IndexOutOfRangeException("Channel count must be between 1 and 511");
                channels = value;
            }
        }

        private byte[] values = new byte[512];

        public byte this[int index]
        {
            get
            {
                if (index < 0 || index >= channels)
                    throw new IndexOutOfRangeException(string.Format("Index must be between 0 and {0}", channels - 1));
                return values[index];
            }
            set
            {
                if (index < 0 || index >= channels)
                    throw new IndexOutOfRangeException(string.Format("Index must be between 0 and {0}", channels - 1));
                lock (values)
                {
                    values[index] = value;
                }
            }
        }
    }

    internal class FtdiDmx32 : FtdiDmx
    {
        #region Imports

        [DllImport("FTD2XX.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_Open(short intDeviceNumber, ref int lngHandle);
        [DllImport("FTD2XX.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_Close(int lngHandle);
        [DllImport("FTD2XX.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_SetDivisor(int lngHandle, int div);
        [DllImport("FTD2XX.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_Read(int lngHandle, string lpszBuffer, int lngBufferSize, ref int lngBytesReturned);
        [DllImport("FTD2XX.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_Write(int lngHandle, string lpszBuffer, int lngBufferSize, ref int lngBytesWritten);
        [DllImport("FTD2XX.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_Write(int lngHandle, IntPtr lpBuffer, int lngBufferSize, ref int lngBytesWritten);
        [DllImport("FTD2XX.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_SetBaudRate(int lngHandle, int lngBaudRate);
        [DllImport("FTD2XX.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_SetDataCharacteristics(int lngHandle, byte byWordLength, byte byStopBits, byte byParity);
        [DllImport("FTD2XX.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_SetFlowControl(int lngHandle, short intFlowControl, byte byXonChar, byte byXoffChar);
        [DllImport("FTD2XX.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_ResetDevice(int lngHandle);
        [DllImport("FTD2XX.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_SetDtr(int lngHandle);
        [DllImport("FTD2XX.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_ClrDtr(int lngHandle);
        [DllImport("FTD2XX.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_SetRts(int lngHandle);
        [DllImport("FTD2XX.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_ClrRts(int lngHandle);
        [DllImport("FTD2XX.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_GetModemStatus(int lngHandle, ref int lngModemStatus);
        [DllImport("FTD2XX.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_Purge(int lngHandle, int lngMask);
        [DllImport("FTD2XX.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_GetStatus(int lngHandle, ref int lngRxBytes, ref int lngTxBytes, ref int lngEventsDWord);
        [DllImport("FTD2XX.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_GetQueueStatus(int lngHandle, ref int lngRxBytes);
        [DllImport("FTD2XX.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_GetEventStatus(int lngHandle, ref int lngEventsDWord);
        [DllImport("FTD2XX.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_SetChars(int lngHandle, byte byEventChar, byte byEventCharEnabled, byte byErrorChar, byte byErrorCharEnabled);
        [DllImport("FTD2XX.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_SetTimeouts(int lngHandle, int lngReadTimeout, int lngWriteTimeout);
        [DllImport("FTD2XX.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_SetBreakOn(int lngHandle);
        [DllImport("FTD2XX.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_SetBreakOff(int lngHandle);

        // FTDI Constants
        private const short FT_OK = 0;
        private const short FT_INVALID_HANDLE = 1;
        private const short FT_DEVICE_NOT_FOUND = 2;
        private const short FT_DEVICE_NOT_OPENED = 3;
        private const short FT_IO_ERROR = 4;
        private const short FT_INSUFFICIENT_RESOURCES = 5;

        // Word Lengths
        private const byte FT_BITS_8 = 8;
        // Stop Bits
        private const byte FT_STOP_BITS_2 = 2;
        // Parity
        private const byte FT_PARITY_NONE = 0;
        // Flow Control
        private const byte FT_FLOW_NONE = 0x0;
        // Purge rx and tx buffers
        private const byte FT_PURGE_RX = 1;
        private const byte FT_PURGE_TX = 2;

        #endregion

        int handle = 0;

        public override void Open()
        {
            short any = 0;
            handle = 0;

            if (State != FtdiDmxState.Closed)
                throw new FtdiDmxException("Already open");

            if (FT_Open(any, ref handle) != FT_OK)
                throw new FtdiDmxException("FTTD not found");

            try
            {

                if (FT_ResetDevice(handle) != FT_OK)
                    throw new FtdiDmxException("Failed to reset device");

                if (FT_SetDivisor(handle, 12) != FT_OK)
                    throw new FtdiDmxException("Failed to set baud rate");

                if (FT_SetDataCharacteristics(handle, FT_BITS_8, FT_STOP_BITS_2, FT_PARITY_NONE) != FT_OK)
                    throw new FtdiDmxException("Failed to set data characteristics");

                if (FT_SetFlowControl(handle, FT_FLOW_NONE, 0, 0) != FT_OK)
                    throw new FtdiDmxException("Failed to set flow control");

                if (FT_ClrRts(handle) != FT_OK)
                    throw new FtdiDmxException("Failed to set RS485 to send");

                if (FT_Purge(handle, FT_PURGE_TX) != FT_OK)
                    throw new FtdiDmxException("Failed to purge TX buffer");

                if (FT_Purge(handle, FT_PURGE_RX) != FT_OK)
                    throw new FtdiDmxException("Failed to purge RX buffer");

                State = FtdiDmxState.Open;
            }
            catch
            {
                State = FtdiDmxState.Closed;
                FT_Close(handle);
                throw;
            }
        }

        public override void Close()
        {
            StopRunning();
            FT_Close(handle);
            handle = 0;
            State = FtdiDmxState.Closed;
        }

        protected override int WriteData(IntPtr dataPtr, int dataLen)
        {
            FT_SetBreakOn(handle);
            Thread.Sleep(10);
            FT_SetBreakOff(handle);
            int written = 0;            
            FT_Write(handle, dataPtr, dataLen, ref written);
            return written;
        }
    }

    internal class FtdiDmx64 : FtdiDmx
    {
        #region Imports

        [DllImport("FTD2XX64.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_Open(short intDeviceNumber, ref int lngHandle);
        [DllImport("FTD2XX64.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_Close(int lngHandle);
        [DllImport("FTD2XX64.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_SetDivisor(int lngHandle, int div);
        [DllImport("FTD2XX64.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_Read(int lngHandle, string lpszBuffer, int lngBufferSize, ref int lngBytesReturned);
        [DllImport("FTD2XX64.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_Write(int lngHandle, string lpszBuffer, int lngBufferSize, ref int lngBytesWritten);
        [DllImport("FTD2XX64.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_Write(int lngHandle, IntPtr lpBuffer, int lngBufferSize, ref int lngBytesWritten);
        [DllImport("FTD2XX64.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_SetBaudRate(int lngHandle, int lngBaudRate);
        [DllImport("FTD2XX64.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_SetDataCharacteristics(int lngHandle, byte byWordLength, byte byStopBits, byte byParity);
        [DllImport("FTD2XX64.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_SetFlowControl(int lngHandle, short intFlowControl, byte byXonChar, byte byXoffChar);
        [DllImport("FTD2XX64.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_ResetDevice(int lngHandle);
        [DllImport("FTD2XX64.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_SetDtr(int lngHandle);
        [DllImport("FTD2XX64.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_ClrDtr(int lngHandle);
        [DllImport("FTD2XX64.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_SetRts(int lngHandle);
        [DllImport("FTD2XX64.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_ClrRts(int lngHandle);
        [DllImport("FTD2XX64.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_GetModemStatus(int lngHandle, ref int lngModemStatus);
        [DllImport("FTD2XX64.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_Purge(int lngHandle, int lngMask);
        [DllImport("FTD2XX64.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_GetStatus(int lngHandle, ref int lngRxBytes, ref int lngTxBytes, ref int lngEventsDWord);
        [DllImport("FTD2XX64.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_GetQueueStatus(int lngHandle, ref int lngRxBytes);
        [DllImport("FTD2XX64.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_GetEventStatus(int lngHandle, ref int lngEventsDWord);
        [DllImport("FTD2XX64.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_SetChars(int lngHandle, byte byEventChar, byte byEventCharEnabled, byte byErrorChar, byte byErrorCharEnabled);
        [DllImport("FTD2XX64.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_SetTimeouts(int lngHandle, int lngReadTimeout, int lngWriteTimeout);
        [DllImport("FTD2XX64.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_SetBreakOn(int lngHandle);
        [DllImport("FTD2XX64.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int FT_SetBreakOff(int lngHandle);

        // FTDI Constants
        private const short FT_OK = 0;
        private const short FT_INVALID_HANDLE = 1;
        private const short FT_DEVICE_NOT_FOUND = 2;
        private const short FT_DEVICE_NOT_OPENED = 3;
        private const short FT_IO_ERROR = 4;         
        private const short FT_INSUFFICIENT_RESOURCES = 5;

        // Word Lengths
        private const byte FT_BITS_8 = 8;
        // Stop Bits
        private const byte FT_STOP_BITS_2 = 2;
        // Parity
        private const byte FT_PARITY_NONE = 0;
        // Flow Control
        private const byte FT_FLOW_NONE = 0x0;
        // Purge rx and tx buffers
        private const byte FT_PURGE_RX = 1;        
        private const byte FT_PURGE_TX = 2;

        #endregion

        private int handle = 0;

        public override void Open()
        {
            short any = 0;

            if (State != FtdiDmxState.Closed)
                throw new FtdiDmxException("Already open");

            if (FT_Open(any, ref handle) != FT_OK)
                throw new FtdiDmxException("FTTD not found");

            try
            {
            if (FT_ResetDevice(handle) != FT_OK)
                throw new FtdiDmxException("Failed to reset device");

            if (FT_SetDivisor(handle, 12) != FT_OK)
                throw new FtdiDmxException("Failed to set baud rate");

            if (FT_SetDataCharacteristics(handle, FT_BITS_8, FT_STOP_BITS_2, FT_PARITY_NONE) != FT_OK)
                throw new FtdiDmxException("Failed to set data characteristics");

            if (FT_SetFlowControl(handle, FT_FLOW_NONE, 0, 0) != FT_OK)
                throw new FtdiDmxException("Failed to set flow control");

            if (FT_ClrRts(handle) != FT_OK)
                throw new FtdiDmxException("Failed to set RS485 to send");

            if (FT_Purge(handle, FT_PURGE_TX) != FT_OK)
                throw new FtdiDmxException("Failed to purge TX buffer");

            if (FT_Purge(handle, FT_PURGE_RX) != FT_OK)
                throw new FtdiDmxException("Failed to purge RX buffer");

                State = FtdiDmxState.Open;
            }
            catch
            {
                State = FtdiDmxState.Closed;
                FT_Close(handle);
                throw;
            }
        }

        public override void Close()
        {
            StopRunning();
            FT_Close(handle);
            State = FtdiDmxState.Closed;
        }

        protected override int WriteData(IntPtr dataPtr, int dataLen)
        {
            FT_SetBreakOn(handle);
            FT_SetBreakOff(handle);
            int written = 0;
            FT_Write(handle, dataPtr, dataLen, ref written);
            return written;
        }
    }
}
