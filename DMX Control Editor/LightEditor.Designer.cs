﻿namespace DmxControlEditor
{
    partial class LightEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.redValue = new System.Windows.Forms.NumericUpDown();
            this.redChange = new System.Windows.Forms.NumericUpDown();
            this.valueLabel = new System.Windows.Forms.Label();
            this.changeLabel = new System.Windows.Forms.Label();
            this.greenChange = new System.Windows.Forms.NumericUpDown();
            this.greenValue = new System.Windows.Forms.NumericUpDown();
            this.blueChange = new System.Windows.Forms.NumericUpDown();
            this.blueValue = new System.Windows.Forms.NumericUpDown();
            this.redPresent = new System.Windows.Forms.CheckBox();
            this.greenPresent = new System.Windows.Forms.CheckBox();
            this.bluePresent = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.redValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.redChange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.greenChange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.greenValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blueChange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blueValue)).BeginInit();
            this.SuspendLayout();
            // 
            // redValue
            // 
            this.redValue.Increment = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.redValue.Location = new System.Drawing.Point(68, 36);
            this.redValue.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.redValue.Name = "redValue";
            this.redValue.Size = new System.Drawing.Size(60, 23);
            this.redValue.TabIndex = 4;
            this.redValue.ValueChanged += new System.EventHandler(this.redValue_ValueChanged);
            // 
            // redChange
            // 
            this.redChange.Increment = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.redChange.Location = new System.Drawing.Point(68, 65);
            this.redChange.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.redChange.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.redChange.Name = "redChange";
            this.redChange.Size = new System.Drawing.Size(60, 23);
            this.redChange.TabIndex = 8;
            this.redChange.ValueChanged += new System.EventHandler(this.redChange_ValueChanged);
            // 
            // valueLabel
            // 
            this.valueLabel.AutoSize = true;
            this.valueLabel.Location = new System.Drawing.Point(11, 38);
            this.valueLabel.Name = "valueLabel";
            this.valueLabel.Size = new System.Drawing.Size(39, 15);
            this.valueLabel.TabIndex = 3;
            this.valueLabel.Text = "&Value:";
            // 
            // changeLabel
            // 
            this.changeLabel.AutoSize = true;
            this.changeLabel.Location = new System.Drawing.Point(11, 67);
            this.changeLabel.Name = "changeLabel";
            this.changeLabel.Size = new System.Drawing.Size(51, 15);
            this.changeLabel.TabIndex = 7;
            this.changeLabel.Text = "&Change:";
            // 
            // greenChange
            // 
            this.greenChange.Increment = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.greenChange.Location = new System.Drawing.Point(134, 65);
            this.greenChange.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.greenChange.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.greenChange.Name = "greenChange";
            this.greenChange.Size = new System.Drawing.Size(60, 23);
            this.greenChange.TabIndex = 9;
            this.greenChange.ValueChanged += new System.EventHandler(this.greenChange_ValueChanged);
            // 
            // greenValue
            // 
            this.greenValue.Increment = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.greenValue.Location = new System.Drawing.Point(134, 36);
            this.greenValue.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.greenValue.Name = "greenValue";
            this.greenValue.Size = new System.Drawing.Size(60, 23);
            this.greenValue.TabIndex = 5;
            this.greenValue.ValueChanged += new System.EventHandler(this.greenValue_ValueChanged);
            // 
            // blueChange
            // 
            this.blueChange.Increment = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.blueChange.Location = new System.Drawing.Point(200, 65);
            this.blueChange.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.blueChange.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.blueChange.Name = "blueChange";
            this.blueChange.Size = new System.Drawing.Size(60, 23);
            this.blueChange.TabIndex = 10;
            this.blueChange.ValueChanged += new System.EventHandler(this.blueChange_ValueChanged);
            // 
            // blueValue
            // 
            this.blueValue.Increment = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.blueValue.Location = new System.Drawing.Point(200, 36);
            this.blueValue.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.blueValue.Name = "blueValue";
            this.blueValue.Size = new System.Drawing.Size(60, 23);
            this.blueValue.TabIndex = 6;
            this.blueValue.ValueChanged += new System.EventHandler(this.blueValue_ValueChanged);
            // 
            // redPresent
            // 
            this.redPresent.AutoSize = true;
            this.redPresent.Location = new System.Drawing.Point(68, 11);
            this.redPresent.Name = "redPresent";
            this.redPresent.Size = new System.Drawing.Size(46, 19);
            this.redPresent.TabIndex = 0;
            this.redPresent.Text = "&Red";
            this.redPresent.UseVisualStyleBackColor = true;
            this.redPresent.CheckedChanged += new System.EventHandler(this.redPresent_CheckedChanged);
            // 
            // greenPresent
            // 
            this.greenPresent.AutoSize = true;
            this.greenPresent.Location = new System.Drawing.Point(134, 11);
            this.greenPresent.Name = "greenPresent";
            this.greenPresent.Size = new System.Drawing.Size(57, 19);
            this.greenPresent.TabIndex = 1;
            this.greenPresent.Text = "&Green";
            this.greenPresent.UseVisualStyleBackColor = true;
            this.greenPresent.CheckedChanged += new System.EventHandler(this.greenPresent_CheckedChanged);
            // 
            // bluePresent
            // 
            this.bluePresent.AutoSize = true;
            this.bluePresent.Location = new System.Drawing.Point(200, 11);
            this.bluePresent.Name = "bluePresent";
            this.bluePresent.Size = new System.Drawing.Size(49, 19);
            this.bluePresent.TabIndex = 2;
            this.bluePresent.Text = "&Blue";
            this.bluePresent.UseVisualStyleBackColor = true;
            this.bluePresent.CheckedChanged += new System.EventHandler(this.bluePresent_CheckedChanged);
            // 
            // LightEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.bluePresent);
            this.Controls.Add(this.greenPresent);
            this.Controls.Add(this.redPresent);
            this.Controls.Add(this.blueChange);
            this.Controls.Add(this.blueValue);
            this.Controls.Add(this.greenChange);
            this.Controls.Add(this.greenValue);
            this.Controls.Add(this.changeLabel);
            this.Controls.Add(this.valueLabel);
            this.Controls.Add(this.redChange);
            this.Controls.Add(this.redValue);
            this.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "LightEditor";
            this.Padding = new System.Windows.Forms.Padding(8);
            this.Size = new System.Drawing.Size(271, 99);
            ((System.ComponentModel.ISupportInitialize)(this.redValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.redChange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.greenChange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.greenValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blueChange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blueValue)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown redValue;
        private System.Windows.Forms.NumericUpDown redChange;
        private System.Windows.Forms.Label valueLabel;
        private System.Windows.Forms.Label changeLabel;
        private System.Windows.Forms.NumericUpDown greenChange;
        private System.Windows.Forms.NumericUpDown greenValue;
        private System.Windows.Forms.NumericUpDown blueChange;
        private System.Windows.Forms.NumericUpDown blueValue;
        private System.Windows.Forms.CheckBox redPresent;
        private System.Windows.Forms.CheckBox greenPresent;
        private System.Windows.Forms.CheckBox bluePresent;
    }
}
