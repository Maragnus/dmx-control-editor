﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DmxControlEditor
{
    public partial class LightEditor : UserControl
    {
        public LightEditor(DmxTimeBlock timeBlock, DmxModule module)
        {
            InitializeComponent();

            TimeBlock = timeBlock;
            Module = module;

            timeBlock.PropertyChanged += TimeBlock_PropertyChanged;
            module.PropertyChanged += Module_PropertyChanged;
            UpdateEverything();
            Program.EnableSelectAllOnFocus(this);
        }

        private bool changing = false;
        void UpdateEverything()
        {
            changing = true;

            var red = TimeBlock.Values.FirstOrDefault(m => m.Index == Module.Index1);
            redPresent.Checked = red == null ? false : red.Present;
            redValue.Enabled = redPresent.Checked;
            redValue.Value = red == null || !red.Present ? 0 : red.Value;
            redChange.Enabled = redPresent.Checked;
            redChange.Value = red == null || !red.Present ? 0 : red.Change;

            if (Module.Type == ModuleType.RGB || Module.Type == ModuleType.GRB)
            {
                redPresent.Text = "&Red";

                var green = TimeBlock.Values.FirstOrDefault(m => m.Index == Module.Index2);
                greenPresent.Checked = green == null ? false : green.Present;
                greenValue.Enabled = greenPresent.Checked;
                greenValue.Value = green == null || !green.Present ? 0 : green.Value;
                greenChange.Enabled = greenPresent.Checked;
                greenChange.Value = green == null || !green.Present ? 0 : green.Change;

                var blue = TimeBlock.Values.FirstOrDefault(m => m.Index == Module.Index3);
                bluePresent.Checked = blue == null ? false : blue.Present;
                blueValue.Enabled = bluePresent.Checked;
                blueValue.Value = blue == null || !blue.Present ? 0 : blue.Value;
                blueChange.Enabled = bluePresent.Checked;
                blueChange.Value = blue == null || !blue.Present ? 0 : blue.Change;
            }
            else
            {
                redPresent.Text = "&Module";
            }

            redPresent.Visible = Module.Type != ModuleType.None;
            redValue.Visible = Module.Type != ModuleType.None;
            redChange.Visible = Module.Type != ModuleType.None;

            greenPresent.Visible = Module.Type == ModuleType.RGB || Module.Type == ModuleType.GRB;
            greenValue.Visible = Module.Type == ModuleType.RGB || Module.Type == ModuleType.GRB;
            greenChange.Visible = Module.Type == ModuleType.RGB || Module.Type == ModuleType.GRB;
            bluePresent.Visible = Module.Type == ModuleType.RGB || Module.Type == ModuleType.GRB;
            blueValue.Visible = Module.Type == ModuleType.RGB || Module.Type == ModuleType.GRB;
            blueChange.Visible = Module.Type == ModuleType.RGB || Module.Type == ModuleType.GRB;

            changing = false;
        }

        void Module_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            UpdateEverything();
        }

        void TimeBlock_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            UpdateEverything();
        }

        private DmxTimeBlock TimeBlock;
        private DmxModule Module;

        private void redPresent_CheckedChanged(object sender, EventArgs e)
        {
            if (changing) return;
            var check = redPresent.Checked;
            TimeBlock[Module.Index1].Present = check;
        }

        private void greenPresent_CheckedChanged(object sender, EventArgs e)
        {
            if (changing) return;
            var check = greenPresent.Checked;
            TimeBlock[Module.Index2].Present = check;
            
        }

        private void bluePresent_CheckedChanged(object sender, EventArgs e)
        {
            if (changing) return;
            var check = bluePresent.Checked;
            TimeBlock[Module.Index3].Present = check;
        }

        private void redValue_ValueChanged(object sender, EventArgs e)
        {
            if (changing) return;
            TimeBlock[Module.Index1].Value = (int)redValue.Value;
        }

        private void greenValue_ValueChanged(object sender, EventArgs e)
        {
            if (changing) return;
            TimeBlock[Module.Index2].Value = (int)greenValue.Value;

        }

        private void blueValue_ValueChanged(object sender, EventArgs e)
        {
            if (changing) return;
            TimeBlock[Module.Index3].Value = (int)blueValue.Value;

        }

        private void redChange_ValueChanged(object sender, EventArgs e)
        {
            if (changing) return;
            TimeBlock[Module.Index1].Change = (int)redChange.Value;
        }

        private void greenChange_ValueChanged(object sender, EventArgs e)
        {
            if (changing) return;
            TimeBlock[Module.Index2].Change = (int)greenChange.Value;
        }

        private void blueChange_ValueChanged(object sender, EventArgs e)
        {
            if (changing) return;
            TimeBlock[Module.Index3].Change = (int)blueChange.Value;
        }
    }
}
