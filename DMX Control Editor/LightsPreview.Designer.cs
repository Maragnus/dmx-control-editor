﻿namespace DmxControlEditor
{
    partial class LightsPreview
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.previewTimer = new System.Windows.Forms.Timer(this.components);
            this.addButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // previewTimer
            // 
            this.previewTimer.Enabled = true;
            this.previewTimer.Interval = 25;
            this.previewTimer.Tick += new System.EventHandler(this.previewTimer_Tick);
            // 
            // addButton
            // 
            this.addButton.Image = global::DmxControlEditor.Properties.Resources.add;
            this.addButton.Location = new System.Drawing.Point(27, 10);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 0;
            this.addButton.TabStop = true;
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // LightsPreview
            // 
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.addButton);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.Name = "LightsPreview";
            this.Size = new System.Drawing.Size(386, 23);
            this.Load += new System.EventHandler(this.LightsPreview_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.LightsPreview_Paint);
            this.Layout += new System.Windows.Forms.LayoutEventHandler(this.LightsPreview_Layout);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.LightsPreview_MouseClick);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer previewTimer;
        private System.Windows.Forms.Button addButton;
    }
}
