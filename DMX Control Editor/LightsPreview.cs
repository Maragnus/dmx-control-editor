﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.Layout;

namespace DmxControlEditor
{
    public partial class LightsPreview : UserControl
    {
        public LightsPreview()
        {
            InitializeComponent();
            startTime = DateTime.Now;
        }

        private DateTime startTime;

        private DmxControl control;
        public DmxControl DmxControl
        {
            get { return control; }
            set
            {
                if (DmxControl != null)
                    DmxControl.PropertyChanged -= DmxControl_PropertyChanged;
                control = value;
                if (DmxControl != null)
                    DmxControl.PropertyChanged += DmxControl_PropertyChanged;

                UpdateModules();
            }
        }

        void DmxControl_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "Modules")
                return;
            UpdateModules();
        }
        
        void editModule(DmxModule module)
        {
            try
            {
                Rectangle box = ClientRectangle;
                modules.TryGetValue(module, out box);
                var popup = new PopupForm()
                {
                    Location = PointToScreen(new Point(box.Left, box.Bottom))
                };
                popup.SuspendLayout();
                popup.Controls.Add(new ModuleEditor() { Module = module, Location = new Point(0, 0), Margin = new Padding(0) });
                popup.ResumeLayout();
                popup.Show(ParentForm);
                popup.CloseOnDeactivate = true;
            }
            catch
            { }
        }

        public void UpdateModules()
        {
            try
            {
                var moduleCount = (DmxControl == null) ? 0 : DmxControl.Modules.Count;
                this.modules = new Dictionary<DmxModule, Rectangle>(moduleCount);

                int left = 4, top = 4, width = this.Width;
                for (var index = 0; index <= moduleCount; index++)
                {
                    if (left + ModuleWidth + ModulePadding > width)
                    {
                        top += ModuleHeight + ModulePadding;
                        left = ModulePadding;
                    }
                    var rect = new Rectangle(left, top, ModuleWidth, ModuleHeight);
                    left += ModuleWidth + ModulePadding;

                    if (index < moduleCount)
                    {
                        var module = DmxControl.Modules[index];
                        this.modules[module] = rect;
                    }
                    else
                    {
                        addButton.Bounds = rect;
                    }
                }
            }
            catch
            { }
        }

        void addButton_Click(object sender, EventArgs e)
        {
            try
            {
                var module = new DmxModule(DmxControl);
                var lastIndex = DmxControl.GetLastIndex();
                module.Type = modules.Any() ? modules.Last().Key.Type : ModuleType.RGB;
                if (module.Type == ModuleType.RGB)
                {
                    module.Index1 = lastIndex + 1;
                    module.Index2 = lastIndex + 2;
                    module.Index3 = lastIndex + 3;
                    module.Color = Color.White;
                }
                if (module.Type == ModuleType.GRB)
                {
                    module.Index2 = lastIndex + 1;
                    module.Index1 = lastIndex + 2;
                    module.Index3 = lastIndex + 3;
                    module.Color = Color.White;
                }
                else if (module.Type == ModuleType.Single)
                {
                    module.Index1 = lastIndex + 1;
                    module.Color = modules.Any() ? modules.Last().Key.Color : Color.White;
                }
                DmxControl.AddModule(module);

                UpdateModules();
                Invalidate();
                editModule(module);
            }
            catch
            { }
        }

        private void previewTimer_Tick(object sender, EventArgs e)
        {
            if (DmxControl == null)
                return;

            if (modules == null)
                UpdateModules();

            Invalidate();
        }

        private void LightsPreview_Load(object sender, EventArgs e)
        {

        }

        const int ModuleWidth = 24;
        const int ModuleHeight = 24;
        const int ModulePadding = 4;

        private Dictionary<DmxModule, Rectangle> modules;

        public override Size GetPreferredSize(Size proposedSize)
        {
            var moduleCount = 1 + ((DmxControl == null) ? 0 : DmxControl.Modules.Count);
            var columns = (int)Math.Floor((double)proposedSize.Width / (double)(moduleCount + ModuleWidth + ModulePadding));
            var rows = (int)Math.Ceiling((double)moduleCount / (double)columns);
            return base.GetPreferredSize(new Size(proposedSize.Width, rows * (ModuleHeight + ModulePadding) + ModulePadding));
        }

        private void LightsPreview_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                var outlinePen = new Pen(Color.Black);
                var textLight = new SolidBrush(Color.White);
                var textDark = new SolidBrush(Color.Black);
                var textDisabled = new SolidBrush(Color.Gray);
                var textFormat = new StringFormat()
                {
                    Alignment = StringAlignment.Center,
                    LineAlignment = StringAlignment.Center,
                    FormatFlags = StringFormatFlags.NoWrap,
                    Trimming = StringTrimming.None
                };

                var time = (int)((DateTime.Now - startTime).TotalMilliseconds);
                var values = DmxControl.GetValues(time);

                foreach (var it in modules)
                {
                    var module = it.Key;
                    var rect = it.Value;

                    var moduleColor = module.GetColor(values);
                    e.Graphics.FillRectangle(new SolidBrush(moduleColor), rect);

                    var textColor = textLight;
                    if ((moduleColor.R + moduleColor.G + moduleColor.B) / 3.0f > 128.0f)
                        textColor = textDark;
                    e.Graphics.DrawString(module.Name, Font, textColor, rect, textFormat);
                    e.Graphics.DrawRectangle(outlinePen, rect);
                }
            }
            catch
            {

            }
        }

        private void LightsPreview_Layout(object sender, LayoutEventArgs e)
        {
            UpdateModules();
        }

        private void LightsPreview_MouseClick(object sender, MouseEventArgs e)
        {
            foreach (var module in modules)
            {
                if (module.Value.Contains(e.Location))
                {
                    editModule(module.Key);
                    return;
                }
            }
        }
    }
}
