﻿namespace DmxControlEditor
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.eventList = new System.Windows.Forms.ListBox();
            this.newVersionPanel = new System.Windows.Forms.Panel();
            this.newVersionLabel = new System.Windows.Forms.Label();
            this.updateLink = new System.Windows.Forms.LinkLabel();
            this.moduleContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newModulesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openModulesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveModulesAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.automaticRenumberToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enterNumberOfModulesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.addModulesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.newToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.outputDmxSignal = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.lightsToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.previewConditionList = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.helpToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.eventEditor = new DmxControlEditor.EventEditor();
            this.lightsPreview = new DmxControlEditor.LightsPreview();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.newVersionPanel.SuspendLayout();
            this.moduleContextMenu.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.newVersionPanel);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.lightsPreview);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(776, 358);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(776, 383);
            this.toolStripContainer1.TabIndex = 1;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip);
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer.IsSplitterFixed = true;
            this.splitContainer.Location = new System.Drawing.Point(0, 54);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.eventList);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.eventEditor);
            this.splitContainer.Size = new System.Drawing.Size(776, 304);
            this.splitContainer.SplitterDistance = 213;
            this.splitContainer.TabIndex = 5;
            // 
            // eventList
            // 
            this.eventList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eventList.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.eventList.FormattingEnabled = true;
            this.eventList.IntegralHeight = false;
            this.eventList.Location = new System.Drawing.Point(0, 0);
            this.eventList.Name = "eventList";
            this.eventList.Size = new System.Drawing.Size(213, 304);
            this.eventList.TabIndex = 0;
            this.eventList.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.eventList_DrawItem);
            this.eventList.MeasureItem += new System.Windows.Forms.MeasureItemEventHandler(this.eventList_MeasureItem);
            this.eventList.SelectedIndexChanged += new System.EventHandler(this.eventList_SelectedIndexChanged);
            // 
            // newVersionPanel
            // 
            this.newVersionPanel.Controls.Add(this.newVersionLabel);
            this.newVersionPanel.Controls.Add(this.updateLink);
            this.newVersionPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.newVersionPanel.Location = new System.Drawing.Point(0, 31);
            this.newVersionPanel.Name = "newVersionPanel";
            this.newVersionPanel.Size = new System.Drawing.Size(776, 23);
            this.newVersionPanel.TabIndex = 4;
            this.newVersionPanel.Visible = false;
            // 
            // newVersionLabel
            // 
            this.newVersionLabel.AutoSize = true;
            this.newVersionLabel.Location = new System.Drawing.Point(190, 3);
            this.newVersionLabel.Name = "newVersionLabel";
            this.newVersionLabel.Size = new System.Drawing.Size(16, 15);
            this.newVersionLabel.TabIndex = 1;
            this.newVersionLabel.Text = "...";
            // 
            // updateLink
            // 
            this.updateLink.AutoSize = true;
            this.updateLink.Location = new System.Drawing.Point(3, 3);
            this.updateLink.Name = "updateLink";
            this.updateLink.Size = new System.Drawing.Size(144, 15);
            this.updateLink.TabIndex = 0;
            this.updateLink.TabStop = true;
            this.updateLink.Text = "A new version is available!";
            this.updateLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.updateLink_LinkClicked);
            // 
            // moduleContextMenu
            // 
            this.moduleContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newModulesToolStripMenuItem,
            this.openModulesToolStripMenuItem,
            this.saveModulesAsToolStripMenuItem,
            this.toolStripMenuItem1,
            this.automaticRenumberToolStripMenuItem,
            this.toolStripMenuItem2,
            this.addToolStripMenuItem});
            this.moduleContextMenu.Name = "moduleContextMenu";
            this.moduleContextMenu.Size = new System.Drawing.Size(193, 126);
            // 
            // newModulesToolStripMenuItem
            // 
            this.newModulesToolStripMenuItem.Name = "newModulesToolStripMenuItem";
            this.newModulesToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.newModulesToolStripMenuItem.Text = "&New Modules...";
            this.newModulesToolStripMenuItem.Click += new System.EventHandler(this.newModulesToolStripMenuItem_Click);
            // 
            // openModulesToolStripMenuItem
            // 
            this.openModulesToolStripMenuItem.Name = "openModulesToolStripMenuItem";
            this.openModulesToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.openModulesToolStripMenuItem.Text = "&Open Modules...";
            this.openModulesToolStripMenuItem.Click += new System.EventHandler(this.openModulesToolStripMenuItem_Click);
            // 
            // saveModulesAsToolStripMenuItem
            // 
            this.saveModulesAsToolStripMenuItem.Name = "saveModulesAsToolStripMenuItem";
            this.saveModulesAsToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.saveModulesAsToolStripMenuItem.Text = "&Save Modules As...";
            this.saveModulesAsToolStripMenuItem.Click += new System.EventHandler(this.saveModulesAsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(189, 6);
            // 
            // automaticRenumberToolStripMenuItem
            // 
            this.automaticRenumberToolStripMenuItem.Name = "automaticRenumberToolStripMenuItem";
            this.automaticRenumberToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.automaticRenumberToolStripMenuItem.Text = "Automatic &Renumber";
            this.automaticRenumberToolStripMenuItem.Click += new System.EventHandler(this.automaticRenumberToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(189, 6);
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.enterNumberOfModulesToolStripMenuItem,
            this.toolStripTextBox1,
            this.addModulesToolStripMenuItem});
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.addToolStripMenuItem.Text = "&Add Multiple Modules";
            // 
            // enterNumberOfModulesToolStripMenuItem
            // 
            this.enterNumberOfModulesToolStripMenuItem.Enabled = false;
            this.enterNumberOfModulesToolStripMenuItem.Name = "enterNumberOfModulesToolStripMenuItem";
            this.enterNumberOfModulesToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.enterNumberOfModulesToolStripMenuItem.Text = "Enter number of modules:";
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBox1.Text = "10";
            // 
            // addModulesToolStripMenuItem
            // 
            this.addModulesToolStripMenuItem.Image = global::DmxControlEditor.Properties.Resources.add;
            this.addModulesToolStripMenuItem.Name = "addModulesToolStripMenuItem";
            this.addModulesToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.addModulesToolStripMenuItem.Text = "Add Modules";
            this.addModulesToolStripMenuItem.Click += new System.EventHandler(this.addModulesToolStripMenuItem_Click);
            // 
            // toolStrip
            // 
            this.toolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripButton,
            this.openToolStripButton,
            this.saveToolStripButton,
            this.toolStripSeparator1,
            this.outputDmxSignal,
            this.toolStripSeparator2,
            this.lightsToolStripButton,
            this.toolStripSeparator,
            this.toolStripLabel1,
            this.previewConditionList,
            this.toolStripSeparator3,
            this.helpToolStripButton});
            this.toolStrip.Location = new System.Drawing.Point(3, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(483, 25);
            this.toolStrip.TabIndex = 0;
            // 
            // newToolStripButton
            // 
            this.newToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripButton.Image")));
            this.newToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripButton.Name = "newToolStripButton";
            this.newToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.newToolStripButton.Text = "&New";
            this.newToolStripButton.Click += new System.EventHandler(this.newToolStripButton_Click);
            // 
            // openToolStripButton
            // 
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripButton.Image")));
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openToolStripButton.Text = "&Open";
            this.openToolStripButton.Click += new System.EventHandler(this.openToolStripButton_Click);
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripButton.Image")));
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveToolStripButton.Text = "&Save";
            this.saveToolStripButton.Click += new System.EventHandler(this.saveToolStripButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // outputDmxSignal
            // 
            this.outputDmxSignal.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.outputDmxSignal.Image = global::DmxControlEditor.Properties.Resources.asterisk_yellow;
            this.outputDmxSignal.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.outputDmxSignal.Name = "outputDmxSignal";
            this.outputDmxSignal.Size = new System.Drawing.Size(23, 22);
            this.outputDmxSignal.Text = "toolStripButton2";
            this.outputDmxSignal.ToolTipText = "Output the preview DMX signals";
            this.outputDmxSignal.Click += new System.EventHandler(this.outputDmxSignal_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // lightsToolStripButton
            // 
            this.lightsToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.lightsToolStripButton.Image = global::DmxControlEditor.Properties.Resources.dmxcntrl_sm;
            this.lightsToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.lightsToolStripButton.Name = "lightsToolStripButton";
            this.lightsToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.lightsToolStripButton.Text = "Lights...";
            this.lightsToolStripButton.Click += new System.EventHandler(this.lightsToolStripButton_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(107, 22);
            this.toolStripLabel1.Text = "Preview Condition:";
            // 
            // previewConditionList
            // 
            this.previewConditionList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.previewConditionList.DropDownWidth = 200;
            this.previewConditionList.Name = "previewConditionList";
            this.previewConditionList.Size = new System.Drawing.Size(200, 25);
            this.previewConditionList.ToolTipText = "Default event to preview for values not present in the selected event.";
            this.previewConditionList.DropDown += new System.EventHandler(this.previewConditionList_DropDown);
            this.previewConditionList.SelectedIndexChanged += new System.EventHandler(this.previewConditionList_SelectedIndexChanged);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // helpToolStripButton
            // 
            this.helpToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.helpToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("helpToolStripButton.Image")));
            this.helpToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.helpToolStripButton.Name = "helpToolStripButton";
            this.helpToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.helpToolStripButton.Text = "He&lp";
            this.helpToolStripButton.Click += new System.EventHandler(this.helpToolStripButton_Click);
            // 
            // eventEditor
            // 
            this.eventEditor.DmxEvent = null;
            this.eventEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eventEditor.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eventEditor.Location = new System.Drawing.Point(0, 0);
            this.eventEditor.Name = "eventEditor";
            this.eventEditor.Size = new System.Drawing.Size(559, 304);
            this.eventEditor.TabIndex = 0;
            this.eventEditor.Load += new System.EventHandler(this.eventEditor_Load);
            // 
            // lightsPreview
            // 
            this.lightsPreview.AutoSize = true;
            this.lightsPreview.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.lightsPreview.ContextMenuStrip = this.moduleContextMenu;
            this.lightsPreview.DmxControl = null;
            this.lightsPreview.Dock = System.Windows.Forms.DockStyle.Top;
            this.lightsPreview.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lightsPreview.Location = new System.Drawing.Point(0, 0);
            this.lightsPreview.Name = "lightsPreview";
            this.lightsPreview.Size = new System.Drawing.Size(776, 31);
            this.lightsPreview.TabIndex = 0;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(776, 383);
            this.Controls.Add(this.toolStripContainer1);
            this.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainWindow";
            this.Text = "DMX Control Editor";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainWindow_FormClosed);
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.Shown += new System.EventHandler(this.MainWindow_Shown);
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ContentPanel.PerformLayout();
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.newVersionPanel.ResumeLayout(false);
            this.newVersionPanel.PerformLayout();
            this.moduleContextMenu.ResumeLayout(false);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private LightsPreview lightsPreview;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton newToolStripButton;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox previewConditionList;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton outputDmxSignal;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton helpToolStripButton;
        private System.Windows.Forms.ContextMenuStrip moduleContextMenu;
        private System.Windows.Forms.ToolStripMenuItem newModulesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openModulesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveModulesAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem automaticRenumberToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton lightsToolStripButton;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.ListBox eventList;
        private EventEditor eventEditor;
        private System.Windows.Forms.Panel newVersionPanel;
        private System.Windows.Forms.Label newVersionLabel;
        private System.Windows.Forms.LinkLabel updateLink;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripMenuItem addModulesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enterNumberOfModulesToolStripMenuItem;

    }
}