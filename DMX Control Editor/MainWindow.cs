﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DmxControlEditor
{
    public partial class MainWindow : Form
    {
        private DmxControl dmx;
        public DmxControl DmxControl
        {
            get
            {
                return dmx;
            }
            set
            {
                if (dmx != null)
                {
                    dmx.Events.CollectionChanged -= Events_CollectionChanged;
                    dmx.PropertyChanged -= DmxControl_PropertyChanged;
                }
                dmx = value;
                if (dmx != null)
                {
                    dmx.LoadModules();
                    dmx.Events.CollectionChanged += Events_CollectionChanged;
                    dmx.PropertyChanged += DmxControl_PropertyChanged;
                    DmxControl_PropertyChanged(null, new PropertyChangedEventArgs(String.Empty));
                    outputDmxSignal.Checked = DmxControl.DmxRunning;
                }
                lightsPreview.DmxControl = dmx;
                UpdateEventList();
            }
        }

        void DmxControl_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            previewConditionList.SelectedItem = DmxControl.RunningEvent;
            if (e.PropertyName == "DmxRunning")
                outputDmxSignal.Checked = DmxControl.DmxRunning;
            if (e.PropertyName == "Module" || e.PropertyName == "Modules")
                DmxControl.SaveModules(false);
        }

        void UpdateEventList()
        {
            previewConditionList.BeginUpdate();
            previewConditionList.Items.Clear();
            foreach (var ev in DmxControl.Events.Where(ev => ev.TimeBlocks.Count > 0))
            {
                previewConditionList.Items.Add(ev);
            }
            previewConditionList.EndUpdate();            

            eventList.BeginUpdate();
            try
            {
                eventList.Items.Clear();
                foreach (var ev in DmxControl.Events.OrderBy(ev => ev.Name)) 
                {
                    eventList.Items.Add(ev);
                }
            }
            finally
            {
                eventList.EndUpdate();
            }
        }

        void Events_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            UpdateEventList();
        }

        public string FileName { get; protected set; }
        
        public MainWindow()
        {
            InitializeComponent();
        }

        void VersionCheck_DownloadStringCompleted(object sender, System.Net.DownloadStringCompletedEventArgs e)
        {
            try
            {
                if (e.Error != null || string.IsNullOrWhiteSpace(e.Result))
                    return;

                var onlineVersion = new Version(e.Result.Trim());
                var thisVersion = System.Reflection.Assembly.GetEntryAssembly().GetName().Version;
                if (onlineVersion > thisVersion)
                {
                    newVersionLabel.Text = string.Format("You are running version {0} and version {1} is now available.", thisVersion, onlineVersion);
                    newVersionPanel.Visible = true;
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void LoadFile(string fileName)
        {
            try
            {
                DmxControl = DmxControl.CreateFrom(fileName);
                this.FileName = fileName;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to open file: " + fileName + "\n\n" + ex.Message, "DMX Control Editor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void MainWindow_Shown(object sender, EventArgs e)
        {
#if DEMO
            newToolStripButton.Enabled = false;
            newToolStripButton.ToolTipText = "Save is not implemented in this version";
            saveToolStripButton.Enabled = false;
            saveToolStripButton.ToolTipText = "Save is not implemented in this version";
#else
            newToolStripButton.Enabled = true;
            saveToolStripButton.Enabled = true;
#endif
            var web = new System.Net.WebClient();
            web.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.BypassCache);
            web.DownloadStringAsync(new Uri("http://dl.maragnus.com/dmx-version.txt"));
            web.DownloadStringCompleted += VersionCheck_DownloadStringCompleted;
        }

        private void eventList_DrawItem(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();
            if (e.Index >= 0)
            {
                var item = (DmxEvent)eventList.Items[e.Index];
                if (item.TimeBlocks.Count > 0)
                {
                    var preview = new Rectangle(e.Bounds.Location.X, e.Bounds.Location.Y+1, e.Bounds.Height - 2, e.Bounds.Height - 2);
                    //e.Graphics.FillRectangle(new SolidBrush(item.PreviewColor), preview);
                    e.Graphics.DrawRectangle(new Pen(Color.Black), preview);
                }
                var textPos = new Point(e.Bounds.X + e.Bounds.Height + 4, e.Bounds.Y);
                e.Graphics.DrawString(item.Name, eventList.Font, new SolidBrush(e.ForeColor), textPos);
            }
            e.DrawFocusRectangle();
        }

        private void eventList_MeasureItem(object sender, MeasureItemEventArgs e)
        {
            var size = e.Graphics.MeasureString("X", Font);
            e.ItemHeight = (int)size.Height + 0;
            e.ItemWidth = 64;
        }

        private void eventList_SelectedIndexChanged(object sender, EventArgs e)
        {
            eventEditor.DmxEvent = eventList.SelectedItem as DmxEvent;
            DmxControl.PreviewEvent = eventList.SelectedItem as DmxEvent;
        }

        private void previewConditionList_SelectedIndexChanged(object sender, EventArgs e)
        {
            DmxControl.RunningEvent = previewConditionList.SelectedItem as DmxEvent;
        }

        private void outputDmxSignal_Click(object sender, EventArgs e)
        {
            try
            {
                DmxControl.DmxRunning = !DmxControl.DmxRunning;
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Unable to communicate with DMX.\n\n" + ex.Message, "DMX Control Editor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void newToolStripButton_Click(object sender, EventArgs e)
        {
            NewFile();
        }

        private void openToolStripButton_Click(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog()
            {
                Title = "Open DMX Control Document",
                Filter = "XML Files|*.xml",
                FileName = FileName
            };
            if (dialog.ShowDialog() != DialogResult.OK)
                return;
            try
            {
                LoadFile(dialog.FileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Unable to open file.\n\n" + ex.Message, "DMX Control Editor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void helpToolStripButton_Click(object sender, EventArgs e)
        {
            using (var dialog = new AboutBox())
                dialog.ShowDialog(this);
        }

        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            DmxControl.SaveModules();
        }

        public void NewFile()
        {
            DmxControl = DmxControl.CreateFrom(new DmxXml.DMX_CONTROL());                        
        }

        private void newModulesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Are you sure that you would like to remove all configured lighting modules?", "DMX Control Editor", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != System.Windows.Forms.DialogResult.Yes)
                return;

            foreach (var module in DmxControl.Modules.ToArray())
                DmxControl.RemoveModule(module);
            DmxControl.AddModule(new DmxModule(DmxControl)
            {
                Type = ModuleType.RGB,
                Name = "P",
                Index1 = 0,
                Index2 = 1,
                Index3 = 2,
                Color = Color.White
            });
            DmxControl.SaveModules();
        }

        private void openModulesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog()
            {
                Title = "Open DMX Modules Document",
                Filter = "XML Files|*.dmxmodules.xml"
            };
            if (dialog.ShowDialog() != DialogResult.OK)
                return;
            try
            {
                DmxControl.LoadModulesFromFile(dialog.FileName);
                DmxControl.SaveModules();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Unable to open file.\n\n" + ex.Message, "DMX Control Editor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void saveModulesAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dialog = new SaveFileDialog()
            {
                Title = "Save DMX Modules Document",
                Filter = "XML Files|*.dmxmodules.xml"
            };
            if (dialog.ShowDialog() != DialogResult.OK)
                return;
            try
            {
                DmxControl.SaveModulesToFile(dialog.FileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Unable to save file.\n\n" + ex.Message, "DMX Control Editor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void saveToolStripButton_Click(object sender, EventArgs e)
        {
            var dialog = new SaveFileDialog()
            {
                Title = "Save DMX Control Document",
                Filter = "XML Files|*.xml",
                FileName = FileName
            };
            if (dialog.ShowDialog() != DialogResult.OK)
                return;
            try
            {
                DmxControl.Save(dialog.FileName);
                FileName = dialog.FileName;
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Unable to save file.\n\n" + ex.Message, "DMX Control Editor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void automaticRenumberToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Are you sure that you would like to renumber all lighting modules?", "DMX Control Editor", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != System.Windows.Forms.DialogResult.Yes)
                return;
            int index = 0;
            foreach (var module in DmxControl.Modules)
            {
                if (module.Type == ModuleType.Single)
                {
                    module.Index1 = index++;
                }
                else if (module.Type == ModuleType.RGB)
                {
                    module.Index1 = index++;
                    module.Index2 = index++;
                    module.Index3 = index++;
                }
                else if (module.Type == ModuleType.GRB)
                {
                    module.Index2 = index++;
                    module.Index1 = index++;
                    module.Index3 = index++;
                }
            }
        }

        private void lightsToolStripButton_Click(object sender, EventArgs e)
        {
            var bounds = lightsToolStripButton.Bounds;
            var pos = toolStrip.PointToScreen(new Point(bounds.Left, bounds.Bottom));
            moduleContextMenu.Show(pos);
        }

        private void updateLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.maragnus.com/dmx.html#changelog");
            //MessageBox.Show(this, "The download link in the email you received when you purchased will provide you with the updated version of this software.", "DMX Control Editor", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
#if DEMO
            Text = "DMX Control Editor (Demo)";
#else
            Text = "DMX Control Editor";
#endif
        }

        private void previewConditionList_DropDown(object sender, EventArgs e)
        {
            previewConditionList.BeginUpdate();
            previewConditionList.Items.Clear();
            foreach (var ev in DmxControl.Events.Where(ev => ev.TimeBlocks.Count > 0).OrderBy(ev=>ev.Name))
            {
                previewConditionList.Items.Add(ev);
            }
            previewConditionList.EndUpdate();   
        }

        private void addModulesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var modules = DmxControl.Modules;

                var count = Convert.ToByte(toolStripTextBox1.Text);
                for (; count > 0; count--)
                {
                    var module = new DmxModule(DmxControl);
                    var lastIndex = DmxControl.GetLastIndex();
                    module.Name = string.Format("{0}", modules.Count+1);
                    module.Type = modules.Any() ? modules.Last().Type : ModuleType.RGB;
                    if (module.Type == ModuleType.RGB)
                    {
                        module.Index1 = lastIndex + 1;
                        module.Index2 = lastIndex + 2;
                        module.Index3 = lastIndex + 3;
                        module.Color = Color.White;
                    }
                    else if (module.Type == ModuleType.GRB)
                    {
                        module.Index2 = lastIndex + 1;
                        module.Index1 = lastIndex + 2;
                        module.Index3 = lastIndex + 3;
                        module.Color = Color.White;
                    }
                    else if (module.Type == ModuleType.Single)
                    {
                        module.Index1 = lastIndex + 1;
                        module.Color = modules.Any() ? modules.Last().Color : Color.White;
                    }
                    DmxControl.AddModule(module);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Please enter a number between 1 and 255 in the text box.\n\n" + ex.Message, "DMX Control Editor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }            
        }

        private void eventEditor_Load(object sender, EventArgs e)
        {

        }
    }
}
