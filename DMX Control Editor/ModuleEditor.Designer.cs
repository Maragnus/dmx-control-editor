﻿namespace DmxControlEditor
{
    partial class ModuleEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.typeRgb = new System.Windows.Forms.RadioButton();
            this.typeSingle = new System.Windows.Forms.RadioButton();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.redLabel = new System.Windows.Forms.Label();
            this.redIndex = new System.Windows.Forms.NumericUpDown();
            this.greenIndex = new System.Windows.Forms.NumericUpDown();
            this.greenLabel = new System.Windows.Forms.Label();
            this.blueIndex = new System.Windows.Forms.NumericUpDown();
            this.blueLabel = new System.Windows.Forms.Label();
            this.singleLabel = new System.Windows.Forms.Label();
            this.singleIndex = new System.Windows.Forms.NumericUpDown();
            this.singleColorLabel = new System.Windows.Forms.Label();
            this.singleColor = new System.Windows.Forms.PictureBox();
            this.typeNone = new System.Windows.Forms.RadioButton();
            this.nameEdit = new System.Windows.Forms.TextBox();
            this.nameLabel = new System.Windows.Forms.Label();
            this.deleteButton = new System.Windows.Forms.Button();
            this.typeGrb = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.redIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.greenIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blueIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.singleIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.singleColor)).BeginInit();
            this.SuspendLayout();
            // 
            // typeRgb
            // 
            this.typeRgb.AutoSize = true;
            this.typeRgb.Location = new System.Drawing.Point(11, 77);
            this.typeRgb.Name = "typeRgb";
            this.typeRgb.Size = new System.Drawing.Size(131, 19);
            this.typeRgb.TabIndex = 3;
            this.typeRgb.TabStop = true;
            this.typeRgb.Text = "RGB (&3-component)";
            this.typeRgb.UseVisualStyleBackColor = true;
            this.typeRgb.CheckedChanged += new System.EventHandler(this.typeRgb_CheckedChanged);
            // 
            // typeSingle
            // 
            this.typeSingle.AutoSize = true;
            this.typeSingle.Location = new System.Drawing.Point(8, 199);
            this.typeSingle.Name = "typeSingle";
            this.typeSingle.Size = new System.Drawing.Size(141, 19);
            this.typeSingle.TabIndex = 10;
            this.typeSingle.TabStop = true;
            this.typeSingle.Text = "Single (&1-component)";
            this.typeSingle.UseVisualStyleBackColor = true;
            this.typeSingle.CheckedChanged += new System.EventHandler(this.typeSingle_CheckedChanged);
            // 
            // redLabel
            // 
            this.redLabel.AutoSize = true;
            this.redLabel.Location = new System.Drawing.Point(32, 138);
            this.redLabel.Name = "redLabel";
            this.redLabel.Size = new System.Drawing.Size(61, 15);
            this.redLabel.TabIndex = 4;
            this.redLabel.Text = "&Red index:";
            // 
            // redIndex
            // 
            this.redIndex.Location = new System.Drawing.Point(35, 156);
            this.redIndex.Maximum = new decimal(new int[] {
            511,
            0,
            0,
            0});
            this.redIndex.Name = "redIndex";
            this.redIndex.Size = new System.Drawing.Size(87, 23);
            this.redIndex.TabIndex = 5;
            this.redIndex.ValueChanged += new System.EventHandler(this.redIndex_ValueChanged);
            // 
            // greenIndex
            // 
            this.greenIndex.Location = new System.Drawing.Point(128, 156);
            this.greenIndex.Maximum = new decimal(new int[] {
            511,
            0,
            0,
            0});
            this.greenIndex.Name = "greenIndex";
            this.greenIndex.Size = new System.Drawing.Size(87, 23);
            this.greenIndex.TabIndex = 7;
            this.greenIndex.ValueChanged += new System.EventHandler(this.greenIndex_ValueChanged);
            // 
            // greenLabel
            // 
            this.greenLabel.AutoSize = true;
            this.greenLabel.Location = new System.Drawing.Point(125, 138);
            this.greenLabel.Name = "greenLabel";
            this.greenLabel.Size = new System.Drawing.Size(72, 15);
            this.greenLabel.TabIndex = 6;
            this.greenLabel.Text = "&Green index:";
            // 
            // blueIndex
            // 
            this.blueIndex.Location = new System.Drawing.Point(221, 156);
            this.blueIndex.Maximum = new decimal(new int[] {
            511,
            0,
            0,
            0});
            this.blueIndex.Name = "blueIndex";
            this.blueIndex.Size = new System.Drawing.Size(87, 23);
            this.blueIndex.TabIndex = 9;
            this.blueIndex.ValueChanged += new System.EventHandler(this.blueIndex_ValueChanged);
            // 
            // blueLabel
            // 
            this.blueLabel.AutoSize = true;
            this.blueLabel.Location = new System.Drawing.Point(218, 138);
            this.blueLabel.Name = "blueLabel";
            this.blueLabel.Size = new System.Drawing.Size(64, 15);
            this.blueLabel.TabIndex = 8;
            this.blueLabel.Text = "&Blue index:";
            // 
            // singleLabel
            // 
            this.singleLabel.AutoSize = true;
            this.singleLabel.Location = new System.Drawing.Point(32, 235);
            this.singleLabel.Name = "singleLabel";
            this.singleLabel.Size = new System.Drawing.Size(38, 15);
            this.singleLabel.TabIndex = 11;
            this.singleLabel.Text = "&Index:";
            // 
            // singleIndex
            // 
            this.singleIndex.Location = new System.Drawing.Point(35, 253);
            this.singleIndex.Maximum = new decimal(new int[] {
            511,
            0,
            0,
            0});
            this.singleIndex.Name = "singleIndex";
            this.singleIndex.Size = new System.Drawing.Size(87, 23);
            this.singleIndex.TabIndex = 12;
            this.singleIndex.ValueChanged += new System.EventHandler(this.singleIndex_ValueChanged);
            // 
            // singleColorLabel
            // 
            this.singleColorLabel.AutoSize = true;
            this.singleColorLabel.Location = new System.Drawing.Point(125, 235);
            this.singleColorLabel.Name = "singleColorLabel";
            this.singleColorLabel.Size = new System.Drawing.Size(39, 15);
            this.singleColorLabel.TabIndex = 13;
            this.singleColorLabel.Text = "&Color:";
            // 
            // singleColor
            // 
            this.singleColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.singleColor.Location = new System.Drawing.Point(128, 253);
            this.singleColor.Name = "singleColor";
            this.singleColor.Size = new System.Drawing.Size(26, 23);
            this.singleColor.TabIndex = 8;
            this.singleColor.TabStop = false;
            this.singleColor.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // typeNone
            // 
            this.typeNone.AutoSize = true;
            this.typeNone.Location = new System.Drawing.Point(11, 43);
            this.typeNone.Name = "typeNone";
            this.typeNone.Size = new System.Drawing.Size(54, 19);
            this.typeNone.TabIndex = 2;
            this.typeNone.TabStop = true;
            this.typeNone.Text = "N&one";
            this.typeNone.UseVisualStyleBackColor = true;
            this.typeNone.CheckedChanged += new System.EventHandler(this.typeNone_CheckedChanged);
            // 
            // nameEdit
            // 
            this.nameEdit.Location = new System.Drawing.Point(56, 11);
            this.nameEdit.Name = "nameEdit";
            this.nameEdit.Size = new System.Drawing.Size(48, 23);
            this.nameEdit.TabIndex = 1;
            this.nameEdit.TextChanged += new System.EventHandler(this.nameEdit_TextChanged);
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(8, 14);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(42, 15);
            this.nameLabel.TabIndex = 0;
            this.nameLabel.Text = "&Name:";
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(122, 10);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(75, 23);
            this.deleteButton.TabIndex = 14;
            this.deleteButton.Text = "Remo&ve";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // typeGrb
            // 
            this.typeGrb.AutoSize = true;
            this.typeGrb.Location = new System.Drawing.Point(11, 102);
            this.typeGrb.Name = "typeGrb";
            this.typeGrb.Size = new System.Drawing.Size(131, 19);
            this.typeGrb.TabIndex = 3;
            this.typeGrb.TabStop = true;
            this.typeGrb.Text = "&GRB (3-component)";
            this.typeGrb.UseVisualStyleBackColor = true;
            this.typeGrb.CheckedChanged += new System.EventHandler(this.typeGrb_CheckedChanged);
            // 
            // ModuleEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.nameEdit);
            this.Controls.Add(this.singleColor);
            this.Controls.Add(this.blueIndex);
            this.Controls.Add(this.blueLabel);
            this.Controls.Add(this.greenIndex);
            this.Controls.Add(this.singleColorLabel);
            this.Controls.Add(this.greenLabel);
            this.Controls.Add(this.singleIndex);
            this.Controls.Add(this.singleLabel);
            this.Controls.Add(this.redIndex);
            this.Controls.Add(this.redLabel);
            this.Controls.Add(this.typeSingle);
            this.Controls.Add(this.typeNone);
            this.Controls.Add(this.typeGrb);
            this.Controls.Add(this.typeRgb);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "ModuleEditor";
            this.Padding = new System.Windows.Forms.Padding(8);
            this.Size = new System.Drawing.Size(319, 287);
            ((System.ComponentModel.ISupportInitialize)(this.redIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.greenIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blueIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.singleIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.singleColor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton typeRgb;
        private System.Windows.Forms.RadioButton typeSingle;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Label redLabel;
        private System.Windows.Forms.NumericUpDown redIndex;
        private System.Windows.Forms.NumericUpDown greenIndex;
        private System.Windows.Forms.Label greenLabel;
        private System.Windows.Forms.NumericUpDown blueIndex;
        private System.Windows.Forms.Label blueLabel;
        private System.Windows.Forms.Label singleLabel;
        private System.Windows.Forms.NumericUpDown singleIndex;
        private System.Windows.Forms.Label singleColorLabel;
        private System.Windows.Forms.PictureBox singleColor;
        private System.Windows.Forms.RadioButton typeNone;
        private System.Windows.Forms.TextBox nameEdit;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.RadioButton typeGrb;
    }
}
