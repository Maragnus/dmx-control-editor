﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DmxControlEditor
{
    public partial class ModuleEditor : UserControl
    {
        public ModuleEditor()
        {
            InitializeComponent();
            Program.EnableSelectAllOnFocus(this);
        }

        private DmxModule module;
        public DmxModule Module
        {
            get
            {
                return module;
            }
            set
            {
                if (module != null)
                    module.PropertyChanged -= DmxModule_PropertyChanged;
                module = value;
                if (module != null)
                    module.PropertyChanged += DmxModule_PropertyChanged;
                UpdateEverything();
            }
        }

        private bool changing = false;
        void DmxModule_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            UpdateEverything();
        }

        void UpdateEverything()
        {
            changing = true;

            typeNone.Checked = Module.Type == ModuleType.None;
            typeRgb.Checked = Module.Type == ModuleType.RGB;
            typeGrb.Checked = Module.Type == ModuleType.GRB;
            typeSingle.Checked = Module.Type == ModuleType.Single;

            nameEdit.Text = Module.Name;

            redLabel.Enabled = typeRgb.Checked || typeGrb.Checked;
            redIndex.Enabled = typeRgb.Checked || typeGrb.Checked;
            greenLabel.Enabled = typeRgb.Checked || typeGrb.Checked;
            greenIndex.Enabled = typeRgb.Checked || typeGrb.Checked;
            blueLabel.Enabled = typeRgb.Checked || typeGrb.Checked;
            blueIndex.Enabled = typeRgb.Checked || typeGrb.Checked;
            redIndex.Value = module.Index1;
            greenIndex.Value = module.Index2;
            blueIndex.Value = module.Index3;

            singleLabel.Enabled = typeSingle.Checked;
            singleIndex.Enabled = typeSingle.Checked;
            singleColorLabel.Enabled = typeSingle.Checked;
            singleColor.Enabled = typeSingle.Checked;
            singleIndex.Value = module.Index1;
            singleColor.BackColor = module.Color;

            changing = false;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            colorDialog1.Color = singleColor.BackColor;
            var popup = ParentForm as PopupForm;
            if (popup != null)
                popup.CloseOnDeactivate = false;

            if (colorDialog1.ShowDialog() == DialogResult.OK)
                Module.Color = colorDialog1.Color;

            ParentForm.Activate();
            if (popup != null)
                popup.CloseOnDeactivate = true;
        }

        private void redIndex_ValueChanged(object sender, EventArgs e)
        {
            if (changing) return;
            module.Index1 = (int)redIndex.Value;
        }

        private void greenIndex_ValueChanged(object sender, EventArgs e)
        {
            if (changing) return;
            module.Index2 = (int)greenIndex.Value;
        }

        private void blueIndex_ValueChanged(object sender, EventArgs e)
        {
            if (changing) return;
            module.Index3 = (int)blueIndex.Value;
        }

        private void singleIndex_ValueChanged(object sender, EventArgs e)
        {
            if (changing) return;
            module.Index1 = (int)singleIndex.Value;
        }

        private void nameEdit_TextChanged(object sender, EventArgs e)
        {
            if (changing) return;
            module.Name = nameEdit.Text;
        }

        private void typeNone_CheckedChanged(object sender, EventArgs e)
        {
            if (changing) return;
            if (typeNone.Checked)
                module.Type = ModuleType.None;
        }

        private void typeRgb_CheckedChanged(object sender, EventArgs e)
        {
            if (changing) return;
            if (typeRgb.Checked)
                module.Type = ModuleType.RGB;
        }

        private void typeGrb_CheckedChanged(object sender, EventArgs e)
        {
            if (changing) return;
            if (typeGrb.Checked)
                module.Type = ModuleType.GRB;
        }

        private void typeSingle_CheckedChanged(object sender, EventArgs e)
        {
            if (changing) return;
            if (typeSingle.Checked)
                module.Type = ModuleType.Single;
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            module.Control.RemoveModule(module);
            ParentForm.Close();
        }
    }
}
