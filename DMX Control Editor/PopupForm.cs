﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DmxControlEditor
{
    class PopupForm : Form
    {
        public PopupForm()
        {
            FormBorderStyle = FormBorderStyle.None;
            TopMost = true;
            AutoSize = true;
            AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            Padding = new Padding(0);
            StartPosition = FormStartPosition.Manual;
            Deactivate += PopupForm_Deactivate;
        }

        void PopupForm_Deactivate(object sender, EventArgs e)
        {
            if (closeOnDeactivate)
                Close();
        }

        bool closeOnDeactivate = false;
        public bool CloseOnDeactivate
        {
            get
            {                
                return closeOnDeactivate;
            }
            set
            {
                closeOnDeactivate = value;
                if (ActiveForm != this)
                    Close();
            }
        }

        private const int CS_DROPSHADOW = 0x00020000;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }
    }
}
