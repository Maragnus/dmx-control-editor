﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DmxControlEditor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] arguments)
        {
            const string DefaultFile64 = @"C:\Program Files (x86)\Artemis\dat\DMXcommands.xml";
            const string DefaultFile32 = @"C:\Program Files\Artemis\dat\DMXcommands.xml";

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var form = new MainWindow();

            // Bring settings up to date
            if (!Properties.Settings.Default.Updated)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.Updated = true;
                Properties.Settings.Default.Save();
            }

            try
            {
                if (arguments.Length == 1)
                    form.LoadFile(arguments.First());
                else if (System.IO.File.Exists(DefaultFile64))
                    form.LoadFile(DefaultFile64);
                else if (System.IO.File.Exists(DefaultFile32))
                    form.LoadFile(DefaultFile32);
                else
                    form.NewFile();
            }
            catch
            {
                form.NewFile();
            }

            Application.Run(form);
        }

        public static void EnableSelectAllOnFocus(Control control)
        {
            if (control is TextBoxBase)
                control.GotFocus += TextBox_GotFocus;
            else if (control is NumericUpDown)
                control.GotFocus += NumericUpDown_GotFocus;

            if (control.Controls.Count > 0)
                foreach (Control c in control.Controls)
                {
                    EnableSelectAllOnFocus(c);
                }
        }

        static void NumericUpDown_GotFocus(object sender, EventArgs e)
        {
            (sender as Control).BeginInvoke((Action)delegate { (sender as NumericUpDown).Select(0, 100); });
        }

        static void TextBox_GotFocus(object sender, EventArgs e)
        {
            (sender as Control).BeginInvoke((Action)delegate { (sender as TextBoxBase).SelectAll(); });
        }
    }
}
