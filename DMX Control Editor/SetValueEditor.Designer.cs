﻿namespace DmxControlEditor
{
    partial class SetValueEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // SetValueEditor
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.DoubleBuffered = true;
            this.Name = "SetValueEditor";
            this.Size = new System.Drawing.Size(0, 0);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.SetValueEditor_DragDrop);
            this.DragOver += new System.Windows.Forms.DragEventHandler(this.SetValueEditor_DragOver);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.SetValueEditor_Paint);
            this.Layout += new System.Windows.Forms.LayoutEventHandler(this.SetValueEditor_Layout);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.SetValueEditor_MouseClick);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.SetValueEditor_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.SetValueEditor_MouseMove);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
