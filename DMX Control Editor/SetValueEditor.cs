﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DmxControlEditor
{
    public partial class SetValueEditor : UserControl
    {
        public DmxControl DmxControl { get; private set; }
        public DmxTimeBlock TimeBlock { get; private set; }

        public class SetValueClickEventArgs : EventArgs
        {
            public DmxModule Module { get; private set; }
            public Rectangle Bounds { get; private set; }

            public SetValueClickEventArgs(DmxModule module, Rectangle bounds)
            {
                Module = module;
                Bounds = bounds;
            }
        }

        public class SetValueDragObject
        {
            public DmxTimeBlock SourceTimeBlock { get; set; }
            public DmxModule SourceModule { get; set; }
        }

        public class SetValueDropEventArgs : EventArgs
        {
            public DmxTimeBlock SourceTimeBlock { get; private set; }
            public DmxModule SourceModule { get; private set; }
            public DmxTimeBlock DestinationTimeBlock { get; private set; }
            public DmxModule DestinationModule { get; private set; }

            public SetValueDropEventArgs(DmxTimeBlock srcTb, DmxModule srcM, DmxTimeBlock dstTb, DmxModule dstM)
            {
                SourceTimeBlock = srcTb;
                SourceModule = srcM;
                DestinationTimeBlock = dstTb;
                DestinationModule = dstM;
            }
        }

        public event EventHandler<SetValueDropEventArgs> ModuleDrop;
        public event EventHandler<SetValueClickEventArgs> ModuleClick;

        public SetValueEditor(DmxTimeBlock timeBlock)
        {
            InitializeComponent();
            TimeBlock = timeBlock;
            DmxControl = timeBlock.Control;
            TimeBlock.PropertyChanged += TimeBlock_PropertyChanged;
            TimeBlock.Control.PropertyChanged += DmxControl_PropertyChanged;
            UpdateModules();
        }

        protected override void OnHandleDestroyed(EventArgs e)
        {
            if (TimeBlock != null)
            {
                TimeBlock.PropertyChanged -= TimeBlock_PropertyChanged;
                TimeBlock.Control.PropertyChanged -= DmxControl_PropertyChanged;
                TimeBlock = null;
            }
            base.OnHandleDestroyed(e);
        }

        void DmxControl_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "Modules")
                return;
            UpdateModules();
        }

        void TimeBlock_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            UpdateModules();
        }

        private void SetValueEditor_Layout(object sender, LayoutEventArgs e)
        {
            UpdateModules();
        }

        private void SetValueEditor_MouseClick(object sender, MouseEventArgs e)
        {
            foreach (var module in modules)
            {
                if (module.Value.Contains(e.Location))
                    if (ModuleClick != null)
                        ModuleClick(this, new SetValueClickEventArgs(module.Key, module.Value));
            }           
        }

        const int ModuleWidth = 22;
        const int ModuleHeight = 22;
        const int ModulePadding = 4;

        private Dictionary<DmxModule, Rectangle> modules;

        public override Size GetPreferredSize(Size proposedSize)
        {
            var moduleCount = ((DmxControl == null) ? 0 : DmxControl.Modules.Count);
            return new Size(ModulePadding + moduleCount * (ModuleWidth + ModulePadding), ModuleHeight + 1);
        }
        
        public void UpdateModules()
        {
            try
            {
                var moduleCount = (DmxControl == null) ? 0 : DmxControl.Modules.Count;
                this.modules = new Dictionary<DmxModule, Rectangle>(moduleCount);

                int left = 0;
                for (var index = 0; index < moduleCount; index++)
                {
                    var rect = new Rectangle(left, 0, ModuleWidth, ModuleHeight);
                    left += ModuleWidth + ModulePadding;

                    var module = DmxControl.Modules[index];
                    this.modules[module] = rect;                    
                }
                PerformLayout();
                Invalidate();
            }
            catch
            { }
        }

        private void SetValueEditor_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                e.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;

                var outlinePen = new Pen(SystemColors.ButtonShadow);
                var textLight = new SolidBrush(Color.White);
                var textDark = new SolidBrush(Color.Black);
                var textDisabled = new SolidBrush(Color.Gray);
                var textFormat = new StringFormat()
                {
                    Alignment = StringAlignment.Center,
                    LineAlignment = StringAlignment.Center,
                    FormatFlags = StringFormatFlags.NoWrap,
                    Trimming = StringTrimming.None
                };

                foreach (var it in modules)
                {
                    var module = it.Key;
                    var rect = it.Value;

                    var startColor = module.GetStartColor(TimeBlock);
                    var endColor = module.GetEndColor(TimeBlock);
                    Brush fillBrush;
                    if (startColor == endColor)
                        fillBrush = new SolidBrush(startColor);
                    else
                        fillBrush = new System.Drawing.Drawing2D.LinearGradientBrush(rect, startColor, endColor, 90.0f);
                    e.Graphics.FillRectangle(fillBrush, rect);

                    var textColor = textLight;
                    if (startColor.A == 0)
                        textColor = textDisabled;
                    else if ((startColor.R + startColor.G + startColor.B) / 3.0f > 128.0f)
                        textColor = textDark;
                    e.Graphics.DrawString(module.Name, Font, textColor, rect, textFormat);
                    e.Graphics.DrawRectangle(outlinePen, rect);
                }
            }
            catch
            {

            }
        }

        #region Drag and Drop

        bool mouseDown;
        Point mouseDownPos;
        DmxModule mouseDownModule;

        private void SetValueEditor_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                var module = GetModuleAt(e.Location);
                if (module == null)
                    return;
                mouseDown = true;
                mouseDownPos = e.Location;
                mouseDownModule = module;
            }
        }

        private void SetValueEditor_MouseMove(object sender, MouseEventArgs e)
        {
            if (!mouseDown || e.Button != System.Windows.Forms.MouseButtons.Left)
            {
                mouseDown = false;
                return;
            }

            if (Math.Abs(mouseDownPos.X - e.Location.X) > 3 || Math.Abs(mouseDownPos.Y - e.Location.Y) > 3)
            {
                var data = new SetValueDragObject()
                {
                    SourceModule = mouseDownModule,
                    SourceTimeBlock = TimeBlock
                };
                DoDragDrop(data, DragDropEffects.Copy);
            }
        }

        public DmxModule GetModuleAt(int x, int y)
        {
            foreach (var module in modules)
            {
                if (module.Value.Contains(x, y))
                    return module.Key;
            }
            return null;
        }

        public DmxModule GetModuleAt(Point location)
        {
            foreach (var module in modules)
            {
                if (module.Value.Contains(location))
                    return module.Key;
            }
            return null;
        }

        private void SetValueEditor_DragDrop(object sender, DragEventArgs e)
        {
            var location = PointToClient(new Point(e.X, e.Y));

            var module = GetModuleAt(location);
            if (module == null || !e.Data.GetDataPresent(typeof(SetValueDragObject)))
                return;

            var data = e.Data.GetData(typeof(SetValueDragObject)) as SetValueDragObject;
            if (data == null || (data.SourceModule == module && data.SourceTimeBlock == TimeBlock))
                return;

            if (ModuleDrop != null)
                ModuleDrop(this, new SetValueDropEventArgs(data.SourceTimeBlock, data.SourceModule, TimeBlock, module));
        }

        private void SetValueEditor_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.None;

            var location = PointToClient(new Point(e.X, e.Y));

            var module = GetModuleAt(location);
            if (module == null || !e.Data.GetDataPresent(typeof(SetValueDragObject)))
                return;

            var data = e.Data.GetData(typeof(SetValueDragObject)) as SetValueDragObject;
            if (data == null || (data.SourceModule == module && data.SourceTimeBlock == TimeBlock))
                return;

            e.Effect = DragDropEffects.Copy;
        }

        #endregion

    }
}
