﻿namespace DmxControlEditor
{
    partial class TimeBlockEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.milliseconds = new System.Windows.Forms.NumericUpDown();
            this.delete = new System.Windows.Forms.Button();
            this.duplicate = new System.Windows.Forms.Button();
            this.moveDown = new System.Windows.Forms.Button();
            this.moveUp = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.milliseconds)).BeginInit();
            this.SuspendLayout();
            // 
            // milliseconds
            // 
            this.milliseconds.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.milliseconds.Location = new System.Drawing.Point(108, 3);
            this.milliseconds.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.milliseconds.Name = "milliseconds";
            this.milliseconds.Size = new System.Drawing.Size(72, 23);
            this.milliseconds.TabIndex = 4;
            this.milliseconds.ValueChanged += new System.EventHandler(this.milliseconds_ValueChanged);
            // 
            // delete
            // 
            this.delete.Image = global::DmxControlEditor.Properties.Resources.cross;
            this.delete.Location = new System.Drawing.Point(78, 3);
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(24, 24);
            this.delete.TabIndex = 3;
            this.delete.UseVisualStyleBackColor = true;
            this.delete.Click += new System.EventHandler(this.delete_Click);
            // 
            // duplicate
            // 
            this.duplicate.Image = global::DmxControlEditor.Properties.Resources.page_copy;
            this.duplicate.Location = new System.Drawing.Point(53, 3);
            this.duplicate.Name = "duplicate";
            this.duplicate.Size = new System.Drawing.Size(24, 24);
            this.duplicate.TabIndex = 2;
            this.duplicate.UseVisualStyleBackColor = true;
            this.duplicate.Click += new System.EventHandler(this.duplicate_Click);
            // 
            // moveDown
            // 
            this.moveDown.Image = global::DmxControlEditor.Properties.Resources.arrow_down;
            this.moveDown.Location = new System.Drawing.Point(28, 3);
            this.moveDown.Name = "moveDown";
            this.moveDown.Size = new System.Drawing.Size(24, 24);
            this.moveDown.TabIndex = 1;
            this.moveDown.UseVisualStyleBackColor = true;
            this.moveDown.Click += new System.EventHandler(this.moveDown_Click);
            // 
            // moveUp
            // 
            this.moveUp.Image = global::DmxControlEditor.Properties.Resources.arrow_up;
            this.moveUp.Location = new System.Drawing.Point(3, 3);
            this.moveUp.Name = "moveUp";
            this.moveUp.Size = new System.Drawing.Size(24, 24);
            this.moveUp.TabIndex = 0;
            this.moveUp.UseVisualStyleBackColor = true;
            this.moveUp.Click += new System.EventHandler(this.moveUp_Click);
            // 
            // TimeBlockEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.milliseconds);
            this.Controls.Add(this.delete);
            this.Controls.Add(this.duplicate);
            this.Controls.Add(this.moveDown);
            this.Controls.Add(this.moveUp);
            this.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "TimeBlockEditor";
            this.Size = new System.Drawing.Size(183, 30);
            this.Enter += new System.EventHandler(this.TimeBlockEditor_Enter);
            this.Leave += new System.EventHandler(this.TimeBlockEditor_Leave);
            ((System.ComponentModel.ISupportInitialize)(this.milliseconds)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button moveUp;
        private System.Windows.Forms.Button moveDown;
        private System.Windows.Forms.Button duplicate;
        private System.Windows.Forms.Button delete;
        private System.Windows.Forms.NumericUpDown milliseconds;
    }
}
