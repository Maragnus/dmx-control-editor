﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace DmxControlEditor
{
    public partial class TimeBlockEditor : UserControl
    {
        public DmxTimeBlock TimeBlock { get; private set; }

        public event EventHandler MoveDownPressed;
        public event EventHandler MoveUpPressed;
        public event EventHandler DeletePressed;
        public event EventHandler DuplicatePressed;

        public TimeBlockEditor(DmxTimeBlock timeBlock)
        {
            InitializeComponent();

            TimeBlock = timeBlock;
            TimeBlock.PropertyChanged += TimeBlock_PropertyChanged;            
            milliseconds.Value = TimeBlock.Milliseconds;

            var editor = new SetValueEditor(timeBlock);
            editor.Top = milliseconds.Top;
            editor.Left = milliseconds.Left + milliseconds.Width + 4;
            editor.ModuleClick += editor_ModuleClick;
            editor.ModuleDrop += editor_ModuleDrop;
            editor.TabStop = false;
            Controls.Add(editor);
            PerformLayout();
            editor.Left = milliseconds.Left + milliseconds.Width + 4;
            
            Program.EnableSelectAllOnFocus(milliseconds);
        }

        void editor_ModuleDrop(object sender, SetValueEditor.SetValueDropEventArgs e)
        {
            DmxModule dstModule = e.DestinationModule, srcModule = e.SourceModule;
            DmxTimeBlock dstTimeBlock = e.DestinationTimeBlock, srcTimeBlock = e.SourceTimeBlock;

            if (dstModule.Type == ModuleType.None || srcModule.Type == ModuleType.None)
                return;

            // RGB to RGB, GRB to GRB
            if ((dstModule.Type == ModuleType.RGB && srcModule.Type == ModuleType.RGB) || (dstModule.Type == ModuleType.GRB && srcModule.Type == ModuleType.GRB))
            {
                dstTimeBlock[dstModule.Index1].Present = srcTimeBlock[srcModule.Index1].Present;
                dstTimeBlock[dstModule.Index2].Present = srcTimeBlock[srcModule.Index2].Present;
                dstTimeBlock[dstModule.Index3].Present = srcTimeBlock[srcModule.Index3].Present;
                dstTimeBlock[dstModule.Index1].Value = srcTimeBlock[srcModule.Index1].Value;
                dstTimeBlock[dstModule.Index2].Value = srcTimeBlock[srcModule.Index2].Value;
                dstTimeBlock[dstModule.Index3].Value = srcTimeBlock[srcModule.Index3].Value;
                dstTimeBlock[dstModule.Index1].Change = srcTimeBlock[srcModule.Index1].Change;
                dstTimeBlock[dstModule.Index2].Change = srcTimeBlock[srcModule.Index2].Change;
                dstTimeBlock[dstModule.Index3].Change = srcTimeBlock[srcModule.Index3].Change;
            }
            // RGB to GRB, GRB to RGB
            if ((dstModule.Type == ModuleType.RGB && srcModule.Type == ModuleType.GRB) || (dstModule.Type == ModuleType.GRB && srcModule.Type == ModuleType.RGB))
            {
                dstTimeBlock[dstModule.Index2].Present = srcTimeBlock[srcModule.Index1].Present;
                dstTimeBlock[dstModule.Index1].Present = srcTimeBlock[srcModule.Index2].Present;
                dstTimeBlock[dstModule.Index3].Present = srcTimeBlock[srcModule.Index3].Present;
                dstTimeBlock[dstModule.Index2].Value = srcTimeBlock[srcModule.Index1].Value;
                dstTimeBlock[dstModule.Index1].Value = srcTimeBlock[srcModule.Index2].Value;
                dstTimeBlock[dstModule.Index3].Value = srcTimeBlock[srcModule.Index3].Value;
                dstTimeBlock[dstModule.Index2].Change = srcTimeBlock[srcModule.Index1].Change;
                dstTimeBlock[dstModule.Index1].Change = srcTimeBlock[srcModule.Index2].Change;
                dstTimeBlock[dstModule.Index3].Change = srcTimeBlock[srcModule.Index3].Change;
            }
            // RGB to Single, GRB to Single
            else if ((dstModule.Type == ModuleType.RGB || dstModule.Type == ModuleType.GRB) && srcModule.Type == ModuleType.Single)
            {
                dstTimeBlock[dstModule.Index1].Present = srcTimeBlock[srcModule.Index1].Present;
                dstTimeBlock[dstModule.Index2].Present = srcTimeBlock[srcModule.Index1].Present;
                dstTimeBlock[dstModule.Index3].Present = srcTimeBlock[srcModule.Index1].Present;
                dstTimeBlock[dstModule.Index1].Value = srcTimeBlock[srcModule.Index1].Value;
                dstTimeBlock[dstModule.Index2].Value = srcTimeBlock[srcModule.Index1].Value;
                dstTimeBlock[dstModule.Index3].Value = srcTimeBlock[srcModule.Index1].Value;
                dstTimeBlock[dstModule.Index1].Change = srcTimeBlock[srcModule.Index1].Change;
                dstTimeBlock[dstModule.Index2].Change = srcTimeBlock[srcModule.Index1].Change;
                dstTimeBlock[dstModule.Index3].Change = srcTimeBlock[srcModule.Index1].Change;
            }
            // Single to RGB, Single to GRB
            else if (dstModule.Type == ModuleType.Single && (srcModule.Type == ModuleType.RGB || srcModule.Type == ModuleType.GRB))
            {
                TimeBlock[dstModule.Index1].Present = TimeBlock[srcModule.Index1].Present || TimeBlock[srcModule.Index2].Present || TimeBlock[srcModule.Index3].Present;
                TimeBlock[dstModule.Index1].Value = TimeBlock[srcModule.Index1].Value;
                TimeBlock[dstModule.Index1].Change = TimeBlock[srcModule.Index1].Change;
            }  
        }

        void editor_ModuleClick(object sender, SetValueEditor.SetValueClickEventArgs e)
        {
            if (e.Module.Type == ModuleType.None)
                return;
            var popup = new PopupForm()
            {
                Location = (sender as Control).PointToScreen(new Point(e.Bounds.Left, e.Bounds.Bottom))
            };
            popup.SuspendLayout();
            popup.Controls.Add(new LightEditor(TimeBlock, e.Module));
            popup.ResumeLayout();
            popup.Show(ParentForm);
            popup.CloseOnDeactivate = true;
        }
        
        protected override void OnHandleDestroyed(EventArgs e)
        {
            if (TimeBlock != null)
            {
                TimeBlock.PropertyChanged -= TimeBlock_PropertyChanged;
                TimeBlock = null;
            }
            base.OnHandleDestroyed(e);
        }

        void TimeBlock_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Milliseconds")
                milliseconds.Value = TimeBlock.Milliseconds;
        }

        private void moveUp_Click(object sender, EventArgs e)
        {
            if (MoveUpPressed != null)
                MoveUpPressed.Invoke(this, e);
        }

        private void moveDown_Click(object sender, EventArgs e)
        {
            if (MoveDownPressed != null)
                MoveDownPressed.Invoke(this, e);
        }

        private void duplicate_Click(object sender, EventArgs e)
        {
            if (DuplicatePressed != null)
                DuplicatePressed.Invoke(this, e);
        }

        private void delete_Click(object sender, EventArgs e)
        {
            if (DeletePressed != null)
                DeletePressed.Invoke(this, e);
        }

        private void milliseconds_ValueChanged(object sender, EventArgs e)
        {
            TimeBlock.Milliseconds = (int)milliseconds.Value;
        }

        public bool MoveUpEnabled
        {
            get { return moveUp.Enabled;  }
            set { moveUp.Enabled = value;  }
        }

        public bool MoveDownEnabled
        {
            get { return moveDown.Enabled;  }
            set { moveDown.Enabled = value;  }
        }

        private void TimeBlockEditor_Enter(object sender, EventArgs e)
        {
            //(this.Parent.Parent as EventEditor).RestoreScrollPosition();
        }

        private void TimeBlockEditor_Leave(object sender, EventArgs e)
        {
            //(this.Parent.Parent as EventEditor).RestoreScrollPosition();
        }
    }
}
