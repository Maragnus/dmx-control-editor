<?
session_name('dmx_dl_counter');
session_start();

$filename = 'DMXControlEditorDemo.zip';

$fn = 'dl-dmx.txt';
$hits = 0;
if (($hits = file_get_contents($fn)) === false)
{
	$hits = 0;
}

if (!isset($_SESSION['dmx_dled']))
{
	if (($fp = @fopen($fn, 'w')) !== false)
	{
		if (flock($fp, LOCK_EX))
		{
			$hits++;
			fwrite($fp, $hits, strlen($hits));
			flock($fp, LOCK_UN);
			$_SESSION['dmx_dled'] = 1;
		}
		fclose($fp);
	}
}

header("Content-type: application/zip");
header("Content-Disposition: attachment; filename=\"$filename\"");
readfile(dirname(__DIR__) . "/downloads/$filename");

?>